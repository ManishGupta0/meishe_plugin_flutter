import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';
import 'package:meishe_plugin_example/src/pages/home_page.dart';
import 'package:permission_handler/permission_handler.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await requestCameraPermission();
  await MeisheEngine.init();

  runApp(const MyApp());
}

Future<void> requestCameraPermission() async {
  var statuses = await [Permission.camera, Permission.microphone].request();

  for (var entry in statuses.entries) {
    log("${entry.key} is ${entry.value.name}");
  }
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: const HomePage(),
    );
  }
}
