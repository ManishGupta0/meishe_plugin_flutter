import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

enum ApplyContext {
  capture,
  timeline,
}

class EffectsBottomSheet extends StatefulWidget {
  const EffectsBottomSheet({
    super.key,
    this.applyContext = ApplyContext.capture,
  });

  final ApplyContext applyContext;

  @override
  State<EffectsBottomSheet> createState() => _EffectsBottomSheetState();
}

class _EffectsBottomSheetState extends State<EffectsBottomSheet>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController = TabController(
    length: 2,
    vsync: this,
  );

  bool _loading = true;
  late List<MeisheEffect> basicEffects;
  late List<MeisheEffect> advanceEffects;
  late List<MeisheAppliedEffect> appliedEffects;

  @override
  void initState() {
    super.initState();

    _loadEffects();
  }

  Future<void> _loadEffects() async {
    basicEffects = await MeisheEffectManager.getBasicEffectsOfType(
      MeisheEffectType.video,
    );
    advanceEffects = await MeisheEffectManager.getInstalledEffectsOfType(
      MeisheEffectType.video,
    );

    appliedEffects =
        await MeisheEffectManager.getAllAppliedEffectOfTypeFromCapture(
      MeisheEffectType.video,
    );

    setState(() => _loading = false);
  }

  Future<void> _selectEffect(MeisheEffect effect) async {
    late MeisheAppliedEffect e;

    final index = appliedEffects.indexWhere((e) => e.name == effect.name);
    bool isApplied = index >= 0;

    if (isApplied) {
      e = appliedEffects[index];
    } else {
      e = await MeisheEffectManager.addEffectToCapture(effect);

      appliedEffects =
          await MeisheEffectManager.getAllAppliedEffectOfTypeFromCapture(
        MeisheEffectType.video,
      );

      setState(() {});
    }

    if (!mounted) return;

    showModalBottomSheet(
      context: context,
      barrierColor: Colors.transparent,
      builder: (context) {
        return EffectParameterBottomSheet(
          effect: e,
          onDelete: _removeEffect,
        );
      },
    );
  }

  Future<void> _removeEffect(MeisheAppliedEffect effect) async {
    await MeisheEffectManager.removeEffectFromCapture(effect);

    appliedEffects =
        await MeisheEffectManager.getAllAppliedEffectOfTypeFromCapture(
      MeisheEffectType.video,
    );

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      showDragHandle: true,
      onClosing: () {},
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (_loading)
              const Center(child: CircularProgressIndicator())
            else ...[
              TabBar(
                controller: _tabController,
                tabs: const [
                  Tab(text: 'Basic'),
                  Tab(text: 'Advance'),
                ],
              ),
              Flexible(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height * 0.25,
                  ),
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      _basicEffects(),
                      _advanceEffects(),
                    ],
                  ),
                ),
              ),
            ],
          ],
        );
      },
    );
  }

  Widget _basicEffects() {
    return Flexible(
      child: GridView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(8.0),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          childAspectRatio: 0.8,
        ),
        children: [
          ...basicEffects.map(
            (effect) {
              bool isApplied = appliedEffects.any((e) => e.name == effect.name);
              return Column(
                children: [
                  InkWell(
                    onTap: () => _selectEffect(effect),
                    child: Container(
                      width: 64,
                      height: 64,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        border: Border.all(
                          color: isApplied ? Colors.green : Colors.white,
                          width: 2,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    effect.name,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _advanceEffects() {
    return Flexible(
      child: GridView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(8.0),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          childAspectRatio: 0.8,
        ),
        children: [
          ...advanceEffects.map(
            (effect) {
              bool isApplied = appliedEffects.any((e) => e.name == effect.name);
              return Column(
                children: [
                  InkWell(
                    onTap: () => _selectEffect(effect),
                    child: Container(
                      width: 64,
                      height: 64,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        border: Border.all(
                          color: isApplied ? Colors.green : Colors.white,
                          width: 2,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 4),
                  Text(effect.name),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}

class EffectParameterBottomSheet extends StatefulWidget {
  const EffectParameterBottomSheet({
    super.key,
    required this.effect,
    this.onDelete,
  });

  final MeisheAppliedEffect effect;
  final void Function(MeisheAppliedEffect effect)? onDelete;

  @override
  State<EffectParameterBottomSheet> createState() =>
      _EffectParameterBottomSheetState();
}

class _EffectParameterBottomSheetState
    extends State<EffectParameterBottomSheet> {
  final List<MeisheEffectParam> _editableParams = [];

  final _filterOfInterest = [
    "Filter Intensity",
    // Image Adjust
    "Shadow", "Contrast", "Saturation", "Brightness", "Vibrance", "Highlight",
    "Exposure",
    // Beauty
    "Strength", "Whitening", "Reddening",
  ];

  @override
  void initState() {
    super.initState();

    for (var param in widget.effect.params.values) {
      if (_filterOfInterest.contains(param!.name)) {
        _editableParams.add(param);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      showDragHandle: true,
      onClosing: () {},
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppBar(
              centerTitle: true,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.transparent,
              title: Text(
                widget.effect.name,
                style: Theme.of(context).textTheme.titleMedium,
              ),
              actions: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    widget.onDelete?.call(widget.effect);
                  },
                  icon: const Icon(Icons.delete),
                ),
              ],
            ),
            const Divider(),
            ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height * 0.2,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    if (_editableParams.isNotEmpty)
                      ..._editableParams.map(
                        (param) {
                          return EffectParamEditor(
                            effect: widget.effect,
                            param: param,
                          );
                        },
                      )
                    else
                      const Text('No Editable Parameter'),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}

class EffectParamEditor extends StatefulWidget {
  const EffectParamEditor({
    super.key,
    required this.effect,
    required this.param,
  });

  final MeisheAppliedEffect effect;
  final MeisheEffectParam param;

  @override
  State<EffectParamEditor> createState() => _EffectParamEditorState();
}

class _EffectParamEditorState extends State<EffectParamEditor> {
  late double _value = widget.effect.paramsValue[widget.param.name]!;

  Future<void> setValue(double value) async {
    await MeisheEffectManager.setCaptureFloatParamValue(
      effect: widget.effect,
      param: widget.param,
      value: widget.param.defaultValue!,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(widget.param.name),
            Row(
              children: [
                Text(_value.toStringAsFixed(1)),
                if (_value != widget.param.defaultValue!)
                  const SizedBox(width: 8),
                if (_value != widget.param.defaultValue!)
                  InkWell(
                    borderRadius: BorderRadius.circular(24),
                    onTap: () async {
                      await setValue(widget.param.defaultValue!);
                      setState(() => _value = widget.param.defaultValue!);
                    },
                    child: const Icon(
                      Icons.refresh_outlined,
                      size: 22,
                    ),
                  ),
              ],
            ),
          ],
        ),
        Slider(
          min: widget.param.minValue!,
          max: widget.param.maxValue!,
          value: _value,
          divisions: 10,
          onChanged: (value) async {
            await setValue(value);
            setState(() => _value = value);
          },
        ),
      ],
    );
  }
}
