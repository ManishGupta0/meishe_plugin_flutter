import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  const CircleButton({
    required this.child,
    this.label,
    this.tooltip,
    this.onPressed,
    this.iconSize = 24,
    this.gap = 0,
    this.color = Colors.white10,
    this.padding = const EdgeInsets.all(8),
    super.key,
  });

  final Widget child;
  final Widget? label;
  final String? tooltip;
  final double iconSize;
  final double gap;
  final Color color;
  final EdgeInsets padding;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconTheme(
          data: IconThemeData(size: iconSize),
          child: InkWell(
            onTap: onPressed,
            customBorder: const StadiumBorder(),
            child: Container(
              padding: padding,
              decoration: BoxDecoration(
                color: color,
                shape: BoxShape.circle,
              ),
              child: SizedBox(
                width: iconSize,
                height: iconSize,
                child: tooltip == null
                    ? child
                    : Tooltip(
                        message: tooltip,
                        child: child,
                      ),
              ),
            ),
          ),
        ),
        SizedBox(height: gap),
        if (label != null) label!,
      ],
    );
  }
}
