import 'package:flutter/material.dart';
import 'package:meishe_plugin_example/src/widgets/custon_action_button.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({
    super.key,
    this.onPressed,
  });

  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CustomActionButton(
        onPressed: () {
          if (onPressed != null) {
            onPressed?.call();
          } else {
            Navigator.of(context).pop();
          }
        },
        icon: const Icon(Icons.chevron_left),
      ),
    );
  }
}
