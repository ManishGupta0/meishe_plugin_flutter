import 'package:flutter/material.dart';

class CustomActionButton extends StatelessWidget {
  const CustomActionButton({
    required this.icon,
    this.onPressed,
    super.key,
  });

  final Widget icon;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          onPressed?.call();
        },
        style: ElevatedButton.styleFrom(
          shape: const CircleBorder(),
          backgroundColor: Colors.white,
          padding: EdgeInsets.zero,
          minimumSize: Size.zero,
        ),
        child: IconTheme(
          data: Theme.of(context).iconTheme.copyWith(
                color: Colors.black,
              ),
          child: icon,
        ),
      ),
    );
  }
}
