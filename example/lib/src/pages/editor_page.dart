import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_box_transform/flutter_box_transform.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

class EditorSettings {
  static double trackHeight = 64;
  static double frameWidth = 64;
  static double trackGap = 4;
  static int minClipLengthInMicroseconds = 1000;
  static double leftSpace = 0.4;
}

class TimelineConstants {
  static double trackHeight = 64;
  static double frameWidth = 64;
  static double trackGap = 4;
  static int minClipLengthInMicroseconds = 1000;
  static double leftSpace = 0.4;
}

Future<List<String>> _pickVideos() async {
  final result = await FilePicker.platform.pickFiles(
    type: FileType.media,
    allowMultiple: true,
  );

  if (result != null) {
    return result.files.map((e) => e.path!).toList();
  }

  return [];
}

/*
class MenuOption extends StatelessWidget {
  const MenuOption({
    super.key,
    required this.icondata,
    required this.label,
    this.onTap,
  });

  final IconData icondata;
  final String label;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Icon(icondata),
            Text(label),
          ],
        ),
      ),
    );
  }
}

class Menu extends StatefulWidget {
  const Menu({
    super.key,
    required this.controller,
    required this.update,
  });

  final NvsEditor controller;
  final void Function() update;

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  late final backButton = MenuOption(
    icondata: Icons.chevron_left,
    label: 'Back',
    onTap: () {
      _menuStack.removeLast();
      setState(() {});
    },
  );

  late final addButton = MenuOption(
    icondata: Icons.add,
    label: 'Add',
    onTap: () {
      _menuStack.add(_addMenu);
      setState(() {});
    },
  );

  late final addClipButton = MenuOption(
    icondata: Icons.add,
    label: 'Add Clip',
    onTap: () async {
      final files = await _pickVideos();
      for (var file in files) {
        widget.controller.addVideoTrackClip(file);
      }

      widget.update();
    },
  );

  late final addAudioButton = MenuOption(
    icondata: Icons.add,
    label: 'Add Audio',
    onTap: () {},
  );

  late final addEffectButton = MenuOption(
    icondata: Icons.add,
    label: 'Add Effect',
    onTap: () {},
  );

  late final deleteClipButton = MenuOption(
    icondata: Icons.delete,
    label: 'Delete Clip',
    onTap: () {
      // widget.controller.removeClip(widget.controller.selectedClip!.id);
    },
  );

  late final resizeClipButton = MenuOption(
    icondata: Icons.cut,
    label: 'Clip Clip',
    onTap: () {},
  );

  final _menuStack = <Function>[];

  List<MenuOption> _defaultMenu() => [
        if (widget.controller.selectedClip == null)
          addButton
        else ...[
          deleteClipButton,
          resizeClipButton,
        ],
      ];

  List<MenuOption> _addMenu() => [
        addClipButton,
        addAudioButton,
        addEffectButton,
      ];

  @override
  void initState() {
    super.initState();

    _menuStack.add(_defaultMenu);
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: ListenableBuilder(
        listenable: widget.controller,
        builder: (context, _) => Row(
          children: [
            if (_menuStack.length > 1) ...[
              backButton,
              const VerticalDivider(width: 0),
            ],
            Expanded(
              child: SingleChildScrollView(
                child: Row(
                  children: _menuStack.last(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
*/
class EditorPage extends StatefulWidget {
  const EditorPage({super.key});

  @override
  State<EditorPage> createState() => _EditorPageState();
}

class _EditorPageState extends State<EditorPage> {
  final _editor = MeisheEditor();
  bool _init = false;

  @override
  void initState() {
    super.initState();

    _setup();
  }

  @override
  void dispose() {
    _editor.dispose();
    super.dispose();
  }

  Future<void> _setup() async {
    _init = await _editor.init(
      width: 720,
      height: 1280,
      aspectRatio: MeisheVector2(x: 16, y: 9),
    );

    setState(() {});
  }

  Future<void> _playTimeline() async {
    if (!await _editor.isTimelinePlaying()) {
      _editor.playTimeline();
    } else if (await _editor.isTimelinePaused()) {
      _editor.resumeTimeline();
    } else {
      _editor.playTimeline();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Video Editor'),
      ),
      body: _init
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                Expanded(
                  flex: 1,
                  child: _preview(),
                ),
                const Divider(height: 0),
                _playbackControl(),
                const Divider(height: 0),
                Expanded(
                  flex: 1,
                  child: _timeline(),
                ),
                _timelineControls(),
              ],
            ),
    );
  }

  Widget _preview() {
    return Stack(
      fit: StackFit.expand,
      children: [],
    );
  }

  Widget _playbackControl() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Expanded(
            child: Text('current'),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.play_arrow),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.pause),
          ),
          const Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text('max'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _timeline() {
    return const TimelineWidget();
  }

  Widget _timelineControls() {
    return Container();
    // return Menu(controller: _editor);
  }
}

class TimelineUtils {
  static double timestampToDisplacement(int timestamp, double fps) {
    return TimelineConstants.trackHeight * fps * timestamp / 1000000;
  }

  static formatDuration(Duration duration) {
    int hour = duration.inHours;
    int minute = duration.inMinutes - hour * 60;
    int seconds = duration.inSeconds - minute * 60 - hour * 3600;
    int milliseconds = duration.inMilliseconds % 1000;

    return [
      [
        if (hour > 0) hour.toString().padLeft(2, '0'),
        minute.toString().padLeft(2, '0'),
        seconds.toString().padLeft(2, '0'),
      ].join(':'),
      if (milliseconds > 0) (milliseconds / 100).round(),
    ].join('.');
  }
}

class TimelineWidget extends StatefulWidget {
  const TimelineWidget({super.key});

  @override
  State<TimelineWidget> createState() => _TimelineWidgetState();
}

class _TimelineWidgetState extends State<TimelineWidget> {
  var _rect = Rect.zero;
  double leftPadding = 0;

  @override
  Widget build(BuildContext context) {
    int fps = 1;

    const duration = Duration(seconds: 4);
    final frameCount = fps * duration.inSeconds;

    return LayoutBuilder(builder: (context, constraints) {
      leftPadding = constraints.maxWidth * TimelineConstants.leftSpace;
      return Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 8),
                    Padding(
                      padding: EdgeInsets.only(
                        left: leftPadding - TimelineConstants.trackHeight / 2,
                      ),
                      child: Row(
                        children: [
                          ...List.generate(
                            frameCount.toInt() + 1,
                            (index) {
                              final d = Duration(
                                microseconds: (1000000 * index ~/ fps),
                              );
                              return _timestamp(d);
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 8),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                          left: leftPadding,
                          right: constraints.maxWidth - leftPadding,
                        ),
                        width: TimelineUtils.timestampToDisplacement(
                          duration.inMicroseconds,
                          fps.toDouble(),
                        ),
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Colors.red,
                            Colors.blue,
                            Colors.green,
                            Colors.blue,
                            Colors.red,
                          ]),
                        ),
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return Stack(
                              children: [
                                if (_rect != Rect.zero)
                                  ClipResizer(rect: _rect),
                                // ClipWidget(
                                //   clip: clip,
                                //   constraints: constraints,
                                //   onTap: (rect) => setState(() => _rect = rect),
                                // ),
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

          // cursor
          Positioned(
            top: 0,
            bottom: 0,
            left: constraints.maxWidth * TimelineConstants.leftSpace,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
              ),
            ),
          )
        ],
      );
    });
  }

  Widget _timestamp(Duration duration) {
    return Container(
      width: TimelineConstants.trackHeight,
      alignment: Alignment.center,
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.white),
      // ),
      child: Text(
        TimelineUtils.formatDuration(duration),
        style: const TextStyle(
          fontSize: 12,
        ),
      ),
    );
  }
}

class ClipWidget extends StatelessWidget {
  const ClipWidget({
    super.key,
    required this.clip,
    required this.constraints,
    this.onTap,
    this.fps = 1,
  });

  final MeisheClip clip;
  final double fps;
  final BoxConstraints constraints;
  final void Function(Rect rect)? onTap;

  @override
  Widget build(BuildContext context) {
    var left = TimelineUtils.timestampToDisplacement(
      clip.inPoint,
      fps,
    );
    var top = constraints.maxHeight / 2;
    var width = TimelineUtils.timestampToDisplacement(
      clip.duration,
      fps,
    );
    var height = TimelineConstants.trackHeight;
    if (clip.trackType == MeisheTrackType.video) {
      top -= TimelineConstants.trackHeight + TimelineConstants.trackGap / 2;
    } else {
      top += TimelineConstants.trackGap / 2;
    }

    return Positioned(
      left: left,
      top: top,
      child: GestureDetector(
        onTap: () {
          onTap?.call(Rect.fromLTWH(left, top, width, height));
        },
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class ClipResizer extends StatefulWidget {
  const ClipResizer({
    super.key,
    required this.rect,
  });

  final Rect rect;

  @override
  State<ClipResizer> createState() => _ClipResizerState();
}

class _ClipResizerState extends State<ClipResizer> {
  late var rect = widget.rect;

  @override
  Widget build(BuildContext context) {
    return TransformableBox(
      rect: rect,
      allowContentFlipping: false,
      allowFlippingWhileResizing: false,
      handleTapSize: 32,
      handleAlignment: HandleAlignment.center,
      draggable: false,
      constraints: BoxConstraints(
        minWidth: 100,
        minHeight: TimelineConstants.trackHeight,
        maxHeight: TimelineConstants.trackHeight,
      ),
      enabledHandles: const {HandlePosition.left, HandlePosition.right},
      visibleHandles: const {HandlePosition.left, HandlePosition.right},
      onChanged: (result, event) {
        setState(() {
          rect = result.rect;
        });
      },
      contentBuilder: (
        BuildContext context,
        Rect rect,
        Flip flip,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            border: Border.all(
              width: 4,
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
        );
      },
    );
  }
}
