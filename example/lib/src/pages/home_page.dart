import 'package:flutter/material.dart';
import 'package:meishe_plugin_example/src/pages/camera_page.dart';
import 'package:meishe_plugin_example/src/pages/editor_page.dart';
import 'package:meishe_plugin_example/src/pages/thumbnail_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page'),
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const ThumbnailPage(),
                ),
              );
            },
            title: const Text("Thumbnail Page"),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const CameraPage(),
                ),
              );
            },
            title: const Text("Camera Page"),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const EditorPage(),
                ),
              );
            },
            title: const Text("Editor Page"),
          ),
        ],
      ),
    );
  }
}
