import 'package:cross_file/cross_file.dart';
import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';
import 'package:meishe_plugin_example/src/widgets/circle_button.dart';
import 'package:meishe_plugin_example/src/widgets/custom_back_button.dart';
import 'package:meishe_plugin_example/src/widgets/effects_bottomsheet.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({super.key});

  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  MeisheCameraController cameraController = MeisheCameraController();

  bool _isRecording = false;
  final _captures = <XFile>[];

  bool _cameraInit = false;

  @override
  void initState() {
    super.initState();

    _setup();
  }

  @override
  void dispose() {
    cameraController.dispose();
    super.dispose();
  }

  Future<void> _setup() async {
    await MeisheEffectManager.removeAllEffectOfTypeFromCapture(
      MeisheEffectType.video,
    );
    await MeisheEffectManager.removeAllEffectOfTypeFromCapture(
      MeisheEffectType.audio,
    );
    await MeisheEffectManager.removeAllEffectOfTypeFromCapture(
      MeisheEffectType.sticker,
    );

    _cameraInit = await cameraController.init();
    cameraController.flipCamera();
    if (_cameraInit) {
      cameraController.startCamera();
    }

    setState(() {});
  }

  Future<void> _switchCamera() async {
    cameraController.flipCamera();
  }

  Future<void> _takePicture() async {
    final file = await cameraController.takePicture();
    _captures.add(file);
    setState(() {});
  }

  Future<void> _startVideoRecording() async {
    cameraController.startVideoRecording();
    setState(() => _isRecording = true);
  }

  Future<void> _stopVideoRecording() async {
    final result = await cameraController.stopVideoRecording();
    _captures.add(result);
    setState(() => _isRecording = false);
  }

  Future<void> _pauseVideoRecording() async {}

  Future<void> _resumeVideoRecording() async {}

  Future<void> _changeAspectRatio() async {
    final result = await showDialog<MeisheVector2>(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: const Text('Choose Aspect Ratio'),
          children: [
            SimpleDialogOption(
              child: const Text('1:1'),
              onPressed: () {
                Navigator.of(context).pop(MeisheVector2(x: 1, y: 1));
              },
            ),
            SimpleDialogOption(
              child: const Text('9:16'),
              onPressed: () {
                Navigator.of(context).pop(MeisheVector2(x: 9, y: 16));
              },
            ),
            SimpleDialogOption(
              child: const Text('16:9'),
              onPressed: () {
                Navigator.of(context).pop(MeisheVector2(x: 16, y: 9));
              },
            ),
            SimpleDialogOption(
              child: const Text('4:3'),
              onPressed: () {
                Navigator.of(context).pop(MeisheVector2(x: 4, y: 3));
              },
            ),
            SimpleDialogOption(
              child: const Text('3:4'),
              onPressed: () {
                Navigator.of(context).pop(MeisheVector2(x: 3, y: 4));
              },
            ),
          ],
        );
      },
    );

    if (result != null) {
      cameraController.setAspectRatio(result);
      setState(() {});
    }
  }

  Future<void> _pickFile() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        alignment: Alignment.topLeft,
        children: [
          Center(child: _cameraPreview()),
          _overlay(),
        ],
      ),
    );
  }

  Widget _cameraPreview() {
    if (!_cameraInit) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          width: constraints.maxWidth,
          height: constraints.maxWidth /
              (cameraController.aspectRatio.x / cameraController.aspectRatio.y),
          child: const MeisheCapturePreview(),
        );
      },
    );
  }

  Widget _overlay() {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _backButton(),
                _recordingTimer(),
                _cameraOptions(),
              ],
            ),
            _bottomArea(),
          ],
        ),
      ),
    );
  }

  Widget _backButton() {
    return const CustomBackButton();
  }

  Widget _recordingTimer() {
    return Text(
      '00:00:00',
      style: Theme.of(context).textTheme.titleLarge!.copyWith(
            color: Colors.white,
          ),
    );
  }

  Widget _cameraOptions() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 32),
        CircleButton(
          onPressed: _switchCamera,
          child: const Icon(
            Icons.cameraswitch,
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 16),
        CircleButton(
          onPressed: _changeAspectRatio,
          child: const Icon(
            Icons.crop,
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 16),
        CircleButton(
          onPressed: _switchCamera,
          child: const Icon(
            Icons.timer_off_outlined,
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  Widget _bottomArea() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text(
          'Double tap to start recording',
          style: TextStyle(color: Colors.grey),
        ),
        const SizedBox(height: 8),
        _bottomRow1(),
        const SizedBox(height: 16),
        _bottomRow2(),
      ],
    );
  }

  Widget _bottomRow1() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _discoverButton(),
        _liveButton(),
        _cameraButton(),
        _filterButton(),
        _effectButton(),
      ],
    );
  }

  Widget _discoverButton() {
    return CircleButton(
      onPressed: () {},
      tooltip: 'Discover',
      child: const Icon(Icons.search, color: Colors.white),
    );
  }

  Widget _liveButton() {
    return CircleButton(
      onPressed: () {},
      tooltip: 'Live',
      child: const Icon(Icons.live_tv, color: Colors.white),
    );
  }

  Widget _cameraButton() {
    return InkWell(
      onTap: () async {
        if (_isRecording) {
          await _stopVideoRecording();
        } else {
          await _takePicture();
        }
      },
      onDoubleTap: _startVideoRecording,
      borderRadius: BorderRadius.circular(55),
      child: Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white,
            width: 4,
          ),
        ),
        child: _isRecording
            ? _videoRecordingStopButton()
            : Container(
                width: 52,
                height: 52,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
              ),
      ),
    );
  }

  Widget _videoRecordingStopButton() {
    return const Icon(
      Icons.stop,
      size: 52,
      color: Colors.white,
    );
  }

  Widget _filterButton() {
    return CircleButton(
      onPressed: () {},
      tooltip: 'Filter',
      child: const Icon(Icons.photo_filter, color: Colors.white),
    );
  }

  Widget _effectButton() {
    return CircleButton(
      onPressed: () {
        showModalBottomSheet(
          context: context,
          barrierColor: Colors.transparent,
          builder: (context) {
            return const EffectsBottomSheet();
          },
        );
      },
      tooltip: 'Effect',
      child: const Icon(Icons.photo_filter, color: Colors.white),
    );
  }

  Widget _bottomRow2() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.centerLeft,
            child: _gallaryButton(),
          ),
        ),
        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.centerRight,
            child: _captures.isNotEmpty ? _actionButton() : Container(),
          ),
        )
      ],
    );
  }

  Widget _gallaryButton() {
    return Badge(
      backgroundColor: Colors.blue,
      label: Text(
        '${_captures.length}',
        style: const TextStyle(color: Colors.white),
      ),
      child: CircleButton(
        padding: const EdgeInsets.all(6),
        onPressed: _isRecording ? null : _pickFile,
        tooltip: 'Gallary',
        child: Container(
          clipBehavior: Clip.hardEdge,
          decoration: const BoxDecoration(shape: BoxShape.circle),
          child: const Icon(Icons.image, color: Colors.white),
        ),
      ),
    );
  }

  Widget _actionButton() {
    return ElevatedButton(
      onPressed: () {},
      style: ElevatedButton.styleFrom(
        elevation: 0,
        backgroundColor: const Color(0xff566090),
        shape: const StadiumBorder(),
        padding: const EdgeInsets.symmetric(
          horizontal: 18,
          vertical: 4,
        ),
      ),
      child: const Text(
        'next',
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
