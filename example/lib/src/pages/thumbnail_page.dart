import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

class ThumbnailPage extends StatefulWidget {
  const ThumbnailPage({super.key});

  @override
  State<ThumbnailPage> createState() => _ThumbnailPageState();
}

class _ThumbnailPageState extends State<ThumbnailPage> {
  String? _file;

  Future<void> _pickFile() async {
    final result = await FilePicker.platform.pickFiles(type: FileType.media);

    setState(() => _file = result?.files.first.path);
  }

  Future<Uint8List?> _generateThumbnail() async {
    return MeisheUtils.getThumbnail(_file!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thumbnail Page'),
      ),
      body: Column(
        children: [
          FilledButton(
            onPressed: _pickFile,
            child: const Text('Pick File'),
          ),
          const SizedBox(height: 16),
          if (_file != null)
            FutureBuilder<Uint8List?>(
              future: _generateThumbnail(),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return const CircularProgressIndicator();
                }

                if (snapshot.data == null) {
                  return const Text('Failed to get Thumbnail');
                }

                return Image.memory(snapshot.data!);
              },
            ),
        ],
      ),
    );
  }
}
