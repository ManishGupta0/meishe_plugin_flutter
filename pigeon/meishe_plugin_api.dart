import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(
  PigeonOptions(
    dartOut: 'lib/src/logic/meishe_api.g.dart',
    javaOut:
        'android/src/main/java/com/example/meishe_plugin/MeishePluginApi.java',
    javaOptions: JavaOptions(
      package: 'com.example.meishe_plugin',
    ),
  ),
)
enum MeisheEngineState {
  stopped,
  capturePreview,
  captureRecording,
  playback,
  seeking,
  compile;
}

enum MeisheCameraLensDirection {
  front,
  back;
}

enum MeisheCameraFlashMode {
  off,
  on,
  auto,
  tourch;
}

enum MeisheTrackType {
  video,
  audio;
}

enum MeisheClipType {
  video,
  audio;
}

enum MeisheTarget {
  capture,
  timeline,
}

enum MeisheEffectType {
  unknown,
  audio,
  video,
  sticker,
  videoTransition,
}

enum MeisheEffectParamType {
  int,
  float,
  bool,
  string,
  unknowm,
}

class MeisheVector2 {
  const MeisheVector2({
    required this.x,
    required this.y,
  });

  final int x;
  final int y;
}

class MeisheRect {
  MeisheRect({
    required this.left,
    required this.right,
    required this.top,
    required this.bottom,
  });

  final double left;
  final double right;
  final double top;
  final double bottom;
}

class MeisheCameraInfo {
  MeisheCameraInfo({
    required this.index,
    required this.lensDirection,
    required this.supportAutoFocus,
    required this.supportContiniousFocus,
    required this.supportAutoExposure,
    required this.supportZoom,
    required this.maxZoom,
    required this.zoomRatios,
    required this.supportFlash,
    required this.supportExposureCompensation,
    required this.minExposureCompensation,
    required this.maxExposureCompensation,
    required this.exposureCompensationStep,
    required this.supportVideoSize,
  });

  final int index;
  final MeisheCameraLensDirection lensDirection;
  final bool supportAutoFocus;
  final bool supportContiniousFocus;
  final bool supportAutoExposure;
  final bool supportZoom;
  final int maxZoom;
  final List<double?> zoomRatios;
  final bool supportFlash;
  final bool supportExposureCompensation;
  final int minExposureCompensation;
  final int maxExposureCompensation;
  final double exposureCompensationStep;
  final List<MeisheVector2?> supportVideoSize;
}

class MeisheClip {
  const MeisheClip({
    required this.index,
    required this.filePath,
    required this.duration,
    required this.clipType,
    required this.inPoint,
    required this.outPoint,
    required this.trimInPoint,
    required this.trimOutPoint,
    required this.trackType,
  });

  final int index;
  final String filePath;
  final int duration;
  final MeisheClipType clipType;
  final int inPoint;
  final int outPoint;
  final int trimInPoint;
  final int trimOutPoint;
  final MeisheTrackType trackType;
}

class MeisheEffectParam {
  MeisheEffectParam({
    required this.name,
    required this.dataType,
    this.maxValue,
    this.minValue,
    this.defaultValue,
    this.defaultBoolVaue,
    this.defaultStringValue,
  });

  final String name;
  final MeisheEffectParamType dataType;
  final double? maxValue;
  final double? minValue;
  final double? defaultValue;
  final bool? defaultBoolVaue;
  final String? defaultStringValue;
}

class MeisheEffect {
  MeisheEffect({
    required this.name,
    required this.uuid,
    required this.isBasic,
    required this.effectType,
    required this.params,
  });

  final String name;
  final String uuid;
  final bool isBasic;
  final MeisheEffectType effectType;
  final Map<String?, MeisheEffectParam?> params;
}

class MeisheAppliedEffect {
  MeisheAppliedEffect({
    required this.index,
    required this.name,
    required this.uuid,
    required this.isBasic,
    required this.effectType,
    required this.params,
    required this.paramsValue,
  });

  final int index;
  final String name;
  final String uuid;
  final bool isBasic;
  final MeisheEffectType effectType;
  final Map<String?, MeisheEffectParam?> params;
  final Map<String?, double?> paramsValue;
}

class MeisheTimelineAppliedEffect {
  MeisheTimelineAppliedEffect({
    required this.name,
    required this.uuid,
    required this.isBasic,
    required this.inPoint,
    required this.duration,
    required this.effectType,
    required this.params,
    required this.paramsValue,
  });

  final String name;
  final String uuid;
  final bool isBasic;
  final int inPoint;
  final int duration;
  final MeisheEffectType effectType;
  final Map<String?, MeisheEffectParam?> params;
  final Map<String?, double?> paramsValue;
}

class MeisheVideoTransition {
  MeisheVideoTransition({
    required this.name,
    required this.uuid,
    required this.isBasic,
  });

  final String name;
  final String uuid;
  final bool isBasic;
}

class MeisheAppliedVideoTransition {
  MeisheAppliedVideoTransition({
    required this.index,
    required this.name,
    required this.uuid,
    required this.isBasic,
  });

  final int index;
  final String name;
  final String uuid;
  final bool isBasic;
}

////////////////////////////////////////////////////////////////////////////////
@HostApi()
abstract class MeisheApi {
  bool init();
  void dispose();

  /// Capture
  int getCapturePreviewTextureId();
  List<MeisheCameraInfo> getCameras();
  bool startCamera(int cameraIndex, MeisheVector2 aspectRatio);
  void stopCamera();
  void setFlashMode(MeisheCameraFlashMode flashMode);

  String takePicture(int cameraIndex);

  bool startVideoRecording(int cameraIndex, MeisheVector2 aspectRatio);
  String stopVideoRecording();
  bool pauseVideoRecording();
  bool resumeVideoRecording();
  bool isVideoRecordingPaused();

  /// Editor
  int getEditorPreviewTextureId();

  void createTimeline(int width, int height, MeisheVector2 aspectRatio);
  void deleteTimeline();
  void playTimeline(int startAt);
  void pauseTimeline();
  void resumeTimeline();
  bool isTimelinePaused();
  bool isTimelinePlaying();
  void seekTimeline(int timestamp);

  void startTimelineCompiling();
  void cancelTimelineCompiling();
  void pauseTimelineCompiling();
  void resumeTimelineCompiling();
  bool isTimelineCompilingPaused();

  int getTimelineDuration();

  MeisheClip addClip(MeisheTrackType type, String filePath);
  MeisheClip addImageClip(String filePath);
  void removeClip(MeisheTrackType type, int clipIndex);
  void moveClip(MeisheTrackType type, int clipIndex, int toIndex);
  void splitClip(MeisheTrackType type, int clipIndex, int timestamp);
  void resizeClip(
    MeisheTrackType type,
    int clipIndex,
    int trimInPoint,
    int trimOutPoint,
  );
  MeisheClip getClip(MeisheTrackType type, int clipIndex);
  List<MeisheClip> getClips(MeisheTrackType type);

  // Asset
  MeisheEffect instllAssetOfType(MeisheEffectType type, String filePath);
  void uninstallAssetOfType(MeisheEffectType type, String assetId);
  bool isAssetOfTypeInstalled(MeisheEffectType type, String assetId);

  /// Get Effects
  List<MeisheEffect> getBasicEffectsOfType(MeisheEffectType type);
  List<MeisheEffect> getInstalledEffectsOfType(MeisheEffectType type);

  List<MeisheAppliedEffect> getAllAppliedEffectOfTypeFromCapture(
    MeisheEffectType type,
  );

  List<MeisheTimelineAppliedEffect> getAllAppliedEffectOfTypeFromTimeline(
    MeisheTrackType trackType,
    MeisheEffectType type,
  );

  /// Add Effects
  /// To Capture
  MeisheAppliedEffect addEffectToCapture(MeisheEffect effect);
  void removeEffectFromCapture(MeisheAppliedEffect effect);
  void removeAllEffectOfTypeFromCapture(MeisheEffectType type);

  double getCaptureEffectFloatParamValue(
    MeisheAppliedEffect effect,
    MeisheEffectParam param,
  );

  void setCaptureEffectFloatParamValue(
    MeisheAppliedEffect effect,
    MeisheEffectParam param,
    double value,
  );

  /// To Timeline
  MeisheTimelineAppliedEffect addEffectToTimeline(
    MeisheTrackType trackType,
    MeisheEffect effect,
    int inPoint,
    int duration,
  );

  void removeEffectFromTimeline(
    MeisheTrackType trackType,
    MeisheTimelineAppliedEffect effect,
  );

  void resizeTimelineEffect(
    MeisheTimelineAppliedEffect effect,
    int inPoint,
    int outPoint,
  );

  double getTimelineEffectFloatParamValue(
    MeisheTrackType trackType,
    MeisheTimelineAppliedEffect effect,
    MeisheEffectParam param,
  );

  void setTimelineEffectFloatParamValue(
    MeisheTrackType trackType,
    MeisheTimelineAppliedEffect effect,
    MeisheEffectParam param,
    double value,
  );

  // sticker
  MeisheRect getStickerRectFromCapture(MeisheAppliedEffect effect);
  MeisheVector2 getStickerPositionFromCapture(MeisheAppliedEffect effect);
  void updateCaptureStickerPosition(
    MeisheAppliedEffect effect,
    MeisheVector2 position,
  );

  MeisheRect getStickerRectFromTimeline(MeisheTimelineAppliedEffect effect);
  MeisheVector2 getStickerPositionFromTimeline(
    MeisheTimelineAppliedEffect effect,
  );
  void updateTimelineStickerPosition(
    MeisheTimelineAppliedEffect effect,
    MeisheVector2 position,
  );

  // caption
  // getCaptionInPoint();
  // getCaptionOutPoint();
  // setCaptionInPoint();
  // setCaptionOutPoint();
  // getCaptionText();
  // setCaptionText();
  // isCaptionBold();
  // setCaptionBold();
  // isCaptionItilic();
  // setCaptionItalic();
  // getCaptionFontSize();
  // setCaptionFontSize();

  /// Utils
  void getThumbnail(String filePath, int timestamp);
}

@FlutterApi()
abstract class MeisheApiCallback {
  void engineStateChanged(int state);
  void videoRecordingDuration(int cameraIndex, int duration);
  void playbackTimelinePosition(int timestamp);
  void seekingTimelinePosition(int timestamp);
  void compileTimelineProgress(double progress);
  void compileComplete(String filePath);
  void thumbnailGenerated(String filePath, Uint8List data);
}
