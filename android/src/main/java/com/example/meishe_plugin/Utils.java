package com.example.meishe_plugin;

import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_BOOL_DEF_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_FLOAT_DEF_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_FLOAT_MAX_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_FLOAT_MIN_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_INT_DEF_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_INT_MAX_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_INT_MIN_VAL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_NAME;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_STRING_DEF;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_TYPE;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_TYPE_BOOL;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_TYPE_FLOAT;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_TYPE_STRING;
import static com.meicam.sdk.NvsFxDescription.ParamInfoObject.PARAM_TYPE_INT;

import com.meicam.sdk.NvsFx;
import com.meicam.sdk.NvsFxDescription;
import com.example.meishe_plugin.MeishePluginApi.*;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    static public Map<String, Double> getParametersValues(NvsFx fx) {
        Map<String, MeisheEffectParam> params = getParameters(fx.getDescription());
        Map<String, Double> valuesMap = new HashMap<>();

        for (MeisheEffectParam param : params.values()) {
            if (param.getDataType() == MeisheEffectParamType.FLOAT) {
                valuesMap.put(param.getName(), fx.getFloatVal(param.getName()));
            }
        }

        return valuesMap;
    }

    static public Map<String, MeisheEffectParam> getParameters(NvsFxDescription desc) {
        Map<String, MeisheEffectParam> paramsMap = new HashMap<>();

        for (NvsFxDescription.ParamInfoObject param : desc.getAllParamsInfo()) {
            MeisheEffectParam.Builder builder = new MeisheEffectParam.Builder();

            String type = param.getString(PARAM_TYPE);
            String name = param.getString(PARAM_NAME);

            builder.setName(name);

            switch (type) {
                case PARAM_TYPE_INT:
                    builder
                            .setDataType(MeisheEffectParamType.INT)
                            .setDefaultValue((double) param.getInteger(PARAM_INT_DEF_VAL))
                            .setMinValue((double) param.getInteger(PARAM_INT_MIN_VAL))
                            .setMaxValue((double) param.getInteger(PARAM_INT_MAX_VAL));

                    break;
                case PARAM_TYPE_FLOAT:
                    builder
                            .setDataType(MeisheEffectParamType.FLOAT)
                            .setDefaultValue((double) param.getFloat(PARAM_FLOAT_DEF_VAL))
                            .setMinValue((double) param.getFloat(PARAM_FLOAT_MIN_VAL))
                            .setMaxValue((double) param.getFloat(PARAM_FLOAT_MAX_VAL));
                    break;
                case PARAM_TYPE_BOOL:
                    builder
                            .setDataType(MeisheEffectParamType.BOOL)
                            .setDefaultBoolVaue(param.getBoolean(PARAM_BOOL_DEF_VAL));
                    break;
                case PARAM_TYPE_STRING:
                    builder
                            .setDataType(MeisheEffectParamType.STRING)
                            .setDefaultStringValue(param.getString(PARAM_STRING_DEF));
                    break;
                default:
                    builder.setDataType(MeisheEffectParamType.UNKNOWM);
                    break;
            }

            paramsMap.put(name, builder.build());
        }

        return paramsMap;
    }
}
