package com.example.meishe_plugin;

import static com.meicam.sdk.NvsMultiThumbnailSequenceView.THUMBNAIL_IMAGE_FILLMODE_ASPECTCROP;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.meicam.sdk.NvsClip;
import com.meicam.sdk.NvsMultiThumbnailSequenceView;

import java.util.ArrayList;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

class ClipThumbnailView implements PlatformView {
    Map<String, Object> creationParams;
    public Context context;

    ClipThumbnailView(Map<String, Object> creationParams, Context context) {
        this.creationParams = creationParams;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView() {
        ArrayList<NvsMultiThumbnailSequenceView.ThumbnailSequenceDesc> sequenceDescsArray = new ArrayList<>();

        NvsMultiThumbnailSequenceView.ThumbnailSequenceDesc sequenceDescs = new NvsMultiThumbnailSequenceView.ThumbnailSequenceDesc();

        sequenceDescs.mediaFilePath = creationParams.get("mediaFilePath").toString();
        sequenceDescs.trimIn = Long.parseLong(creationParams.get("trimIn").toString());
        sequenceDescs.trimOut = Long.parseLong(creationParams.get("trimOut").toString());
        sequenceDescs.inPoint = Long.parseLong(creationParams.get("inPoint").toString());
        sequenceDescs.outPoint = Long.parseLong(creationParams.get("outPoint").toString());
        sequenceDescs.stillImageHint = false;
        sequenceDescsArray.add(sequenceDescs);

        double ppm = Double.parseDouble(creationParams.get("ppm").toString());

        NvsMultiThumbnailSequenceView v = new NvsMultiThumbnailSequenceView(context);
        v.setThumbnailSequenceDescArray(sequenceDescsArray);
        v.setThumbnailAspectRatio(1);
        v.setThumbnailAspectRatio(THUMBNAIL_IMAGE_FILLMODE_ASPECTCROP);
        v.setPixelPerMicrosecond(ppm);

        return v;
    }

    @Override
    public void dispose() {
    }
}

class ClipThumbnailViewFactory extends PlatformViewFactory {

    ClipThumbnailViewFactory() {
        super(StandardMessageCodec.INSTANCE);
    }

    @NonNull
    @Override
    public PlatformView create(@NonNull Context context, int id, @Nullable Object args) {
        final Map<String, Object> creationParams = (Map<String, Object>) args;

        return new ClipThumbnailView(creationParams, context);
    }
}

/**
 * MeishePlugin
 */
public class MeishePlugin implements FlutterPlugin, ActivityAware {
    public final static String TAG = "MeishePlugin";
    private FlutterPluginBinding flutterPluginBinding;
    private MeisheApiImpl apiImpl;


    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        this.flutterPluginBinding = binding;

        flutterPluginBinding
                .getPlatformViewRegistry()
                .registerViewFactory(
                        "<MultiThumbnailView>",
                        new ClipThumbnailViewFactory()
                );

    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        this.flutterPluginBinding = null;
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        apiImpl = new MeisheApiImpl(binding.getActivity(), this.flutterPluginBinding);

        MeishePluginApi.MeisheApi.setup(
                this.flutterPluginBinding.getBinaryMessenger(),
                apiImpl
        );
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        onAttachedToActivity(binding);
    }

    @Override
    public void onDetachedFromActivity() {
        onDetachedFromActivity();
    }
}


