package com.example.meishe_plugin;

import com.example.meishe_plugin.MeishePluginApi.*;

import static com.example.meishe_plugin.MeishePlugin.TAG;
import static com.meicam.sdk.NvsAssetPackageManager.ASSET_PACKAGE_STATUS_READY;
import static com.meicam.sdk.NvsAssetPackageManager.ASSET_PACKAGE_TYPE_ANIMATEDSTICKER;
import static com.meicam.sdk.NvsAssetPackageManager.ASSET_PACKAGE_TYPE_VIDEOFX;
import static com.meicam.sdk.NvsAssetPackageManager.ASSET_PACKAGE_TYPE_VIDEOTRANSITION;
import static com.meicam.sdk.NvsStreamingContext.COMPILE_BITRATE_GRADE_MEDIUM;
import static com.meicam.sdk.NvsStreamingContext.COMPILE_VIDEO_RESOLUTION_GRADE_720;
import static com.meicam.sdk.NvsStreamingContext.STREAMING_ENGINE_CAPTURE_FLAG_DONT_USE_SYSTEM_RECORDER;
import static com.meicam.sdk.NvsStreamingContext.STREAMING_ENGINE_CAPTURE_FLAG_ENABLE_TAKE_PICTURE;
import static com.meicam.sdk.NvsStreamingContext.STREAMING_ENGINE_STATE_PLAYBACK;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.Log;

import androidx.annotation.NonNull;

import com.meicam.sdk.NvsAssetPackageManager;
import com.meicam.sdk.NvsAudioClip;
import com.meicam.sdk.NvsAudioResolution;
import com.meicam.sdk.NvsAudioTrack;
import com.meicam.sdk.NvsCaptureAnimatedSticker;
import com.meicam.sdk.NvsCaptureAudioFx;
import com.meicam.sdk.NvsCaptureVideoFx;
import com.meicam.sdk.NvsFx;
import com.meicam.sdk.NvsIconGenerator;
import com.meicam.sdk.NvsRational;
import com.meicam.sdk.NvsSize;
import com.meicam.sdk.NvsStreamingContext;
import com.meicam.sdk.NvsStreamingContext.CaptureDeviceCallback;
import com.meicam.sdk.NvsStreamingContext.CaptureRecordingDurationCallback;
import com.meicam.sdk.NvsStreamingContext.CaptureRecordingStartedCallback;
import com.meicam.sdk.NvsStreamingContext.CapturedPictureCallback;
import com.meicam.sdk.NvsStreamingContext.CompileCallback;
import com.meicam.sdk.NvsStreamingContext.CompileCallback2;
import com.meicam.sdk.NvsStreamingContext.CompileCallback3;
import com.meicam.sdk.NvsStreamingContext.CompileFloatProgressCallback;
import com.meicam.sdk.NvsStreamingContext.HardwareErrorCallback;
import com.meicam.sdk.NvsStreamingContext.PlaybackCallback;
import com.meicam.sdk.NvsStreamingContext.PlaybackCallback2;
import com.meicam.sdk.NvsStreamingContext.SeekingCallback;
import com.meicam.sdk.NvsStreamingContext.StreamingEngineCallback;
import com.meicam.sdk.NvsTimeline;
import com.meicam.sdk.NvsTimelineAnimatedSticker;
import com.meicam.sdk.NvsTimelineVideoFx;
import com.meicam.sdk.NvsVideoClip;
import com.meicam.sdk.NvsVideoFrameInfo;
import com.meicam.sdk.NvsVideoResolution;
import com.meicam.sdk.NvsVideoTrack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.view.TextureRegistry;

public class MeisheApiImpl implements
        MeisheApi,
        StreamingEngineCallback,
        CapturedPictureCallback,
        CaptureDeviceCallback,
        CaptureRecordingStartedCallback,
        CaptureRecordingDurationCallback,
        HardwareErrorCallback,
        PlaybackCallback,
        PlaybackCallback2,
        SeekingCallback,
        CompileCallback,
        CompileCallback2,
        CompileCallback3,
        CompileFloatProgressCallback {

    private Activity activity;
    private final FlutterPlugin.FlutterPluginBinding binding;
    public NvsStreamingContext streamingContext;
    public NvsStreamingContext auxiliaryStreamingContext;
    public NvsAssetPackageManager assetManager;
    public TextureRegistry.SurfaceTextureEntry captureTextureEntry;
    public TextureRegistry.SurfaceTextureEntry editorTextureEntry;
    private MeisheApiCallback nvsCallback;
    public NvsTimeline timeline;
    public NvsVideoTrack videoTrack;
    public NvsAudioTrack audioTrack;

    private CompletableFuture<String> takePictureFuture;
    private String resultFilePath = "";

    public MeisheApiImpl(Activity activity, FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        this.activity = activity;
        this.binding = flutterPluginBinding;
    }

    @NonNull
    @Override
    public Boolean init() {
        streamingContext = NvsStreamingContext.init(activity, null);
        auxiliaryStreamingContext = streamingContext.createAuxiliaryStreamingContext(0);
        assetManager = streamingContext.getAssetPackageManager();

        captureTextureEntry = binding.getTextureRegistry().createSurfaceTexture();
        editorTextureEntry = binding.getTextureRegistry().createSurfaceTexture();

        nvsCallback = new MeisheApiCallback(binding.getBinaryMessenger());

        // callbacks
        streamingContext.setStreamingEngineCallback(this);
        streamingContext.setCapturedPictureCallback(this);
        streamingContext.setCaptureDeviceCallback(this);
        streamingContext.setCaptureRecordingStartedCallback(this);
        streamingContext.setCaptureRecordingDurationCallback(this);
        streamingContext.setHardwareErrorCallback(this);
        streamingContext.setPlaybackCallback(this);
        streamingContext.setPlaybackCallback2(this);
        streamingContext.setSeekingCallback(this);
        streamingContext.setCompileCallback(this);
        streamingContext.setCompileCallback2(this);
        streamingContext.setCompileCallback3(this);
        streamingContext.setCompileFloatProgressCallback(this);

        return true;
    }

    @Override
    public void dispose() {
        streamingContext = null;
        NvsStreamingContext.close();
    }

    @NonNull
    @Override
    public Long getCapturePreviewTextureId() {
        return captureTextureEntry.id();
    }

    @NonNull
    @Override
    public List<MeisheCameraInfo> getCameras() {
        List<MeisheCameraInfo> cameras = new ArrayList<>();

        for (int i = 0; i < streamingContext.getCaptureDeviceCount(); i++) {
            NvsStreamingContext.CaptureDeviceCapability device = streamingContext.getCaptureDeviceCapability(i);


            List<Double> zoomRatios = new ArrayList<>();
            for (float value : device.zoomRatios) {
                zoomRatios.add((double) value);
            }

            List<MeisheVector2> videoSize = new ArrayList<>();
            if (device.supportVideoSize != null) {
                for (NvsSize value : device.supportVideoSize) {
                    MeisheVector2 convertedValue = new MeisheVector2.Builder()
                            .setX((long) value.width)
                            .setY((long) value.height)
                            .build();
                    videoSize.add(convertedValue);
                }
            }

            cameras.add(
                    new MeisheCameraInfo.Builder()
                            .setIndex((long) i)
                            .setLensDirection(streamingContext.isCaptureDeviceBackFacing(i) ? MeisheCameraLensDirection.BACK : MeisheCameraLensDirection.FRONT)
                            .setSupportAutoFocus(device.supportAutoFocus)
                            .setSupportContiniousFocus(device.supportContinuousFocus)
                            .setSupportAutoExposure(device.supportAutoExposure)
                            .setSupportZoom(device.supportZoom)
                            .setMaxZoom((long) device.maxZoom)
                            .setZoomRatios(zoomRatios)
                            .setSupportFlash(device.supportFlash)
                            .setSupportExposureCompensation(device.supportExposureCompensation)
                            .setMinExposureCompensation((long) device.minExposureCompensation)
                            .setMaxExposureCompensation((long) device.maxExposureCompensation)
                            .setExposureCompensationStep((double) device.exposureCompensationStep)
                            .setSupportVideoSize(videoSize)
                            .build()
            );
        }
        return cameras;
    }

    @NonNull
    @Override
    public Boolean startCamera(@NonNull Long cameraIndex, @NonNull MeisheVector2 aspectRatio) {
        streamingContext.connectCapturePreviewWithSurfaceTexture(captureTextureEntry.surfaceTexture());

        return streamingContext.startCapturePreview(cameraIndex.intValue(),
                NvsStreamingContext.VIDEO_CAPTURE_RESOLUTION_GRADE_MEDIUM,
                STREAMING_ENGINE_CAPTURE_FLAG_ENABLE_TAKE_PICTURE |
                        STREAMING_ENGINE_CAPTURE_FLAG_DONT_USE_SYSTEM_RECORDER,
                new NvsRational(aspectRatio.getX().intValue(), aspectRatio.getY().intValue())
        );
    }

    @Override
    public void stopCamera() {
        streamingContext.stop();
    }

    @Override
    public void setFlashMode(@NonNull MeisheCameraFlashMode flashMode) {
//        streamingContext.toggleFlashMode(flashMode.intValue());
    }

    @NonNull
    @Override
    public String takePicture(@NonNull Long cameraIndex) {
        streamingContext.takePicture(cameraIndex.intValue());

        takePictureFuture = new CompletableFuture<>();

        try {
            return takePictureFuture.get();
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    @Override
    public Boolean startVideoRecording(@NonNull Long cameraIndex, @NonNull MeisheVector2 aspectRatio) {
        streamingContext.startCapturePreview(cameraIndex.intValue(),
                NvsStreamingContext.VIDEO_CAPTURE_RESOLUTION_GRADE_MEDIUM,
                STREAMING_ENGINE_CAPTURE_FLAG_DONT_USE_SYSTEM_RECORDER,
                new NvsRational(aspectRatio.getX().intValue(), aspectRatio.getY().intValue())
        );

        final File outputDir = activity.getApplicationContext().getCacheDir();
        try {
            File captureFile = File.createTempFile("Rec_", ".mp4", outputDir);
            resultFilePath = captureFile.getAbsolutePath();
            return streamingContext.startRecording(resultFilePath);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    @Override
    public String stopVideoRecording() {
        streamingContext.stopRecording();
        String result = resultFilePath;
        resultFilePath = "";

        return result;
    }

    @NonNull
    @Override
    public Boolean pauseVideoRecording() {
        return streamingContext.pauseRecording();
    }

    @NonNull
    @Override
    public Boolean resumeVideoRecording() {
        return streamingContext.resumeRecording();
    }

    @NonNull
    @Override
    public Boolean isVideoRecordingPaused() {
        return streamingContext.isRecordingPaused();
    }

    @NonNull
    @Override
    public Long getEditorPreviewTextureId() {
        return editorTextureEntry.id();
    }

    @Override
    public void createTimeline(@NonNull Long width, @NonNull Long height, @NonNull MeisheVector2 aspectRatio) {
        NvsVideoResolution videoEditRes = new NvsVideoResolution();
        videoEditRes.imageWidth = width.intValue();
        videoEditRes.imageHeight = height.intValue();
        videoEditRes.imagePAR = new NvsRational(aspectRatio.getX().intValue(), aspectRatio.getY().intValue()); //pixel ratio, set to 1:1
        NvsRational videoFps = new NvsRational(25, 1); //frame rate, users can set 25 or 30, generally 25.

        NvsAudioResolution audioEditRes = new NvsAudioResolution();
        audioEditRes.sampleRate = 44100; //audio sampling rate, users can set 48000 or 44100
        audioEditRes.channelCount = 2; //count of audio channels

        timeline = streamingContext.createTimeline(videoEditRes, videoFps, audioEditRes);
        streamingContext.connectTimelineWithSurfaceTexture(timeline, editorTextureEntry.surfaceTexture());

        videoTrack = timeline.appendVideoTrack();
        audioTrack = timeline.appendAudioTrack();
    }

    @Override
    public void deleteTimeline() {
        streamingContext.removeTimeline(timeline);
        timeline = null;
    }

    @Override
    public void playTimeline(@NonNull Long startAt) {
        streamingContext.playbackTimeline(
                timeline,
                startAt,
                -1,
                NvsStreamingContext.VIDEO_PREVIEW_SIZEMODE_LIVEWINDOW_SIZE,
                false,
                0
        );
    }

    @Override
    public void pauseTimeline() {
        streamingContext.pausePlayback();
    }

    @Override
    public void resumeTimeline() {
        streamingContext.resumePlayback();
    }

    @NonNull
    @Override
    public Boolean isTimelinePaused() {
        return streamingContext.isPlaybackPaused();
    }

    @NonNull
    @Override
    public Boolean isTimelinePlaying() {
        return streamingContext.getStreamingEngineState() == STREAMING_ENGINE_STATE_PLAYBACK;
    }

    @Override
    public void seekTimeline(@NonNull Long timestamp) {
        streamingContext.seekTimeline(timeline, timestamp, 0, 0);
    }

    @Override
    public void startTimelineCompiling() {
        final File outputDir = activity.getApplicationContext().getCacheDir();
        try {
            File captureFile = File.createTempFile("Res_", ".mp4", outputDir);
            resultFilePath = captureFile.getAbsolutePath();

            streamingContext.compileTimeline(
                    timeline,
                    0,
                    timeline.getDuration(),
                    resultFilePath,
                    COMPILE_VIDEO_RESOLUTION_GRADE_720,
                    COMPILE_BITRATE_GRADE_MEDIUM,
                    0
            );

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void cancelTimelineCompiling() {
    }

    @Override
    public void pauseTimelineCompiling() {
        streamingContext.pauseCompiling();
    }

    @Override
    public void resumeTimelineCompiling() {
        streamingContext.resumeCompiling();
    }

    @NonNull
    @Override
    public Boolean isTimelineCompilingPaused() {
        return streamingContext.isCompilingPaused();
    }

    @NonNull
    @Override
    public Long getTimelineDuration() {
        return timeline.getDuration();
    }

    @NonNull
    @Override
    public MeisheClip addClip(@NonNull MeisheTrackType type, @NonNull String filePath) {
        switch (type) {
            case VIDEO:
                NvsVideoClip videoClip = videoTrack.appendClip(filePath);
                return Converters.convertClip(videoClip);
            case AUDIO:
                NvsAudioClip audioClip = audioTrack.appendClip(filePath);
                return Converters.convertClip(audioClip);
        }

        return null;
    }

    @NonNull
    @Override
    public MeisheClip addImageClip(@NonNull String filePath) {
        NvsVideoClip imageClip = videoTrack.appendClip(filePath, 0, 2000000);
        return Converters.convertClip(imageClip);
    }


    @Override
    public void removeClip(@NonNull MeisheTrackType type, @NonNull Long clipIndex) {
        switch (type) {
            case VIDEO:
                videoTrack.removeClip(clipIndex.intValue(), false);
                break;
            case AUDIO:
                audioTrack.removeClip(clipIndex.intValue(), false);
                break;
        }
    }

    @Override
    public void moveClip(@NonNull MeisheTrackType type, @NonNull Long clipIndex, @NonNull Long toIndex) {
        switch (type) {
            case VIDEO:
                videoTrack.moveClip(clipIndex.intValue(), toIndex.intValue());
                break;
            case AUDIO:
                audioTrack.moveClip(clipIndex.intValue(), toIndex.intValue());
                break;
        }
    }

    @Override
    public void splitClip(@NonNull MeisheTrackType type, @NonNull Long clipIndex, @NonNull Long timestamp) {
        switch (type) {
            case VIDEO:
                videoTrack.splitClip(clipIndex.intValue(), timestamp);
                break;
            case AUDIO:
                audioTrack.splitClip(clipIndex.intValue(), timestamp);
                break;
        }
    }

    @Override
    public void resizeClip(@NonNull MeisheTrackType type, @NonNull Long clipIndex, @NonNull Long trimInPoint, @NonNull Long trimOutPoint) {
        switch (type) {
            case VIDEO:
                NvsVideoClip clip = videoTrack.getClipByIndex(clipIndex.intValue());
                clip.changeTrimInPoint(trimInPoint, true);
                clip.changeTrimOutPoint(trimOutPoint, true);
                break;
            default:
                break;
        }
    }

    @NonNull
    @Override
    public MeisheClip getClip(@NonNull MeisheTrackType type, @NonNull Long clipIndex) {
        switch (type) {
            case VIDEO:
                com.meicam.sdk.NvsVideoClip videoClip = videoTrack.getClipByIndex(clipIndex.intValue());
                return Converters.convertClip(videoClip);
            case AUDIO:
                com.meicam.sdk.NvsAudioClip audioClip = audioTrack.getClipByIndex(clipIndex.intValue());
                return Converters.convertClip(audioClip);
        }

        return null;
    }

    @NonNull
    @Override
    public List<MeisheClip> getClips(@NonNull MeisheTrackType type) {
        List<MeisheClip> clips = new ArrayList<>();

        switch (type) {
            case VIDEO:
                for (int i = 0; i < videoTrack.getClipCount(); i++) {
                    com.meicam.sdk.NvsVideoClip clip = videoTrack.getClipByIndex(i);
                    clips.add(Converters.convertClip(clip));
                }
                break;
            case AUDIO:
                for (int i = 0; i < audioTrack.getClipCount(); i++) {
                    com.meicam.sdk.NvsAudioClip clip = audioTrack.getClipByIndex(i);
                    clips.add(Converters.convertClip(clip));
                }
                break;
        }

        return clips;
    }

    @NonNull
    @Override
    public MeisheEffect instllAssetOfType(@NonNull MeisheEffectType type, @NonNull String filePath) {
        StringBuilder packageId = new StringBuilder();
        int error = 0;

        switch (type) {
            case VIDEO:
                error = assetManager.installAssetPackage(filePath, null, ASSET_PACKAGE_TYPE_VIDEOFX, true, packageId);

                break;
            default:
                break;
        }

        if (error != NvsAssetPackageManager.ASSET_PACKAGE_MANAGER_ERROR_NO_ERROR
                && error != NvsAssetPackageManager.ASSET_PACKAGE_MANAGER_ERROR_ALREADY_INSTALLED) {
            Log.e(TAG, "Failed to install asset package!");
            return null;
        }

        return getInstalledEffectOfType(type, packageId.toString());
    }

    @Override
    public void uninstallAssetOfType(@NonNull MeisheEffectType type, @NonNull String assetId) {
        int error = 0;
        switch (type) {
            case VIDEO:
                error = assetManager.uninstallAssetPackage(assetId, ASSET_PACKAGE_TYPE_VIDEOFX);
                break;
            default:
                break;
        }
        if (error != NvsAssetPackageManager.ASSET_PACKAGE_MANAGER_ERROR_NO_ERROR
                && error != NvsAssetPackageManager.ASSET_PACKAGE_MANAGER_ERROR_ALREADY_INSTALLED) {
            Log.e(TAG, "Failed to uninstall asset package!");
        }
    }

    @NonNull
    @Override
    public Boolean isAssetOfTypeInstalled(@NonNull MeisheEffectType type, @NonNull String assetId) {
        switch (type) {
            case VIDEO:
                return assetManager.getAssetPackageStatus(assetId, ASSET_PACKAGE_TYPE_VIDEOFX) == ASSET_PACKAGE_STATUS_READY;
            default:
                break;
        }
        return false;
    }


    @NonNull
    @Override
    public List<MeisheEffect> getBasicEffectsOfType(@NonNull MeisheEffectType type) {
        List<MeisheEffect> effects = new ArrayList<>();

        switch (type) {
            case VIDEO:
                List<String> videoFxNames = streamingContext.getAllBuiltinVideoFxNames();
                videoFxNames.add(0, "Beauty");

                for (String name : videoFxNames) {
                    MeisheEffect.Builder builder = new MeisheEffect.Builder();
                    com.meicam.sdk.NvsFxDescription desc = streamingContext.getVideoFxDescription(name);

                    effects.add(
                            builder.
                                    setName(name)
                                    .setParams(Utils.getParameters(desc))
                                    .setEffectType(MeisheEffectType.VIDEO)
                                    .setUuid("")
                                    .setIsBasic(true)
                                    .build()
                    );
                }

                break;
            case AUDIO:
                List<String> audioFxNames = streamingContext.getAllBuiltinAudioFxNames();

                for (String name : audioFxNames) {
                    MeisheEffect.Builder builder = new MeisheEffect.Builder();
                    com.meicam.sdk.NvsFxDescription desc = streamingContext.getAudioFxDescription(name);

                    effects.add(
                            builder.setName(name)
                                    .setParams(Utils.getParameters(desc))
                                    .setEffectType(MeisheEffectType.AUDIO)
                                    .setUuid("")
                                    .setIsBasic(true)
                                    .build()
                    );
                }
            case VIDEO_TRANSITION:
                List<String> transitionFxNames = streamingContext.getAllBuiltinVideoTransitionNames();

                for (String name : transitionFxNames) {
                    MeisheEffect.Builder builder = new MeisheEffect.Builder();

                    effects.add(
                            builder.setName(name)
                                    .setParams(new HashMap<>())
                                    .setEffectType(MeisheEffectType.VIDEO_TRANSITION)
                                    .setUuid("")
                                    .setIsBasic(true)
                                    .build()
                    );
                }
            default:
                break;
        }

        return effects;
    }

    private MeisheEffect getInstalledEffectOfType(@NonNull MeisheEffectType type, String uuid) {
        MeisheEffect.Builder builder = new MeisheEffect.Builder();

        switch (type) {
            case VIDEO:
                com.meicam.sdk.NvsCaptureVideoFx videoFx = auxiliaryStreamingContext.appendPackagedCaptureVideoFx(uuid);
                auxiliaryStreamingContext.removeAllCaptureVideoFx();

                return new MeisheEffect.Builder()
                        .setName(videoFx.getDescription().getName())
                        .setParams(Utils.getParameters(videoFx.getDescription()))
                        .setEffectType(MeisheEffectType.VIDEO)
                        .setIsBasic(false)
                        .setUuid(uuid)
                        .build();
            case VIDEO_TRANSITION:
                NvsTimeline t = streamingContext.createTimeline(new NvsVideoResolution(), new NvsRational(25, 1), new NvsAudioResolution());
                NvsVideoTrack vTrack = t.appendVideoTrack();
                com.meicam.sdk.NvsVideoTransition tr = vTrack.setBuiltinTransition(0, "");
                streamingContext.removeTimeline(t);

                return new MeisheEffect.Builder()
                        .setName(tr.getDescription().getName())
                        .setParams(new HashMap<>())
                        .setEffectType(MeisheEffectType.VIDEO_TRANSITION)
                        .setUuid(tr.getVideoTransitionPackageId())
                        .setIsBasic(false)
                        .build();

            case STICKER:
                com.meicam.sdk.NvsCaptureAnimatedSticker stickerFx = auxiliaryStreamingContext.appendCaptureAnimatedSticker(0, 100 * 1000000, uuid);
                auxiliaryStreamingContext.removeAllCaptureAnimatedSticker();

                return new MeisheEffect.Builder()
                        .setName(stickerFx.getDescription().getName())
                        .setParams(Utils.getParameters(stickerFx.getDescription()))
                        .setEffectType(MeisheEffectType.VIDEO)
                        .setIsBasic(false)
                        .setUuid(uuid)
                        .build();

            default:
                break;
        }

        return null;
    }

    @NonNull
    @Override
    public List<MeisheEffect> getInstalledEffectsOfType(@NonNull MeisheEffectType type) {
        List<MeisheEffect> effects = new ArrayList<>();

        switch (type) {
            case VIDEO:
                List<String> videoFxAssets = assetManager.getAssetPackageListOfType(ASSET_PACKAGE_TYPE_VIDEOFX);
                for (String asset : videoFxAssets) {
                    effects.add(getInstalledEffectOfType(type, asset));
                }
                break;
            case VIDEO_TRANSITION:
                List<String> transitionFxAssets = assetManager.getAssetPackageListOfType(ASSET_PACKAGE_TYPE_VIDEOTRANSITION);

            case STICKER:
                List<String> stickerFxAssets = assetManager.getAssetPackageListOfType(ASSET_PACKAGE_TYPE_ANIMATEDSTICKER);
                for (String asset : stickerFxAssets) {
                    effects.add(getInstalledEffectOfType(type, asset));
                }
                break;
            default:
                break;
        }

        return effects;
    }

    @NonNull
    @Override
    public List<MeisheAppliedEffect> getAllAppliedEffectOfTypeFromCapture(@NonNull MeisheEffectType type) {
        List<MeisheAppliedEffect> effects = new ArrayList<>();

        switch (type) {
            case VIDEO:
                for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
                    NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);
                    effects.add(Converters.convertCaptureVideoFx(fx));
                }
                break;
            case STICKER:
                break;
            default:
                break;
        }

        return effects;
    }


    @NonNull
    @Override
    public List<MeisheTimelineAppliedEffect> getAllAppliedEffectOfTypeFromTimeline(@NonNull MeisheTrackType trackType, @NonNull MeisheEffectType type) {
        List<MeisheTimelineAppliedEffect> effects = new ArrayList<>();
        switch (type) {
            case VIDEO:
                NvsTimelineVideoFx fx = timeline.getFirstTimelineVideoFx();

                while (true) {
                    if (fx == null) {
                        break;
                    }
                    effects.add(
                            new MeisheTimelineAppliedEffect.Builder()
                                    .setName(fx.getDescription().getName())
                                    .setUuid(fx.getTimelineVideoFxPackageId())
                                    .setIsBasic(fx.getTimelineVideoFxPackageId().isEmpty())
                                    .setInPoint(fx.getInPoint())
                                    .setDuration(fx.getOutPoint())
                                    .setEffectType(type)
                                    .setParams(Utils.getParameters(fx.getDescription()))
                                    .setParamsValue(Utils.getParametersValues(fx))
                                    .build()
                    );
                    fx = timeline.getNextTimelineVideoFx(fx);
                }
                break;
            case AUDIO:
                break;
        }
        return effects;
    }

    @NonNull
    @Override
    public MeisheAppliedEffect addEffectToCapture(@NonNull MeisheEffect effect) {
        switch (effect.getEffectType()) {
            case VIDEO:
                NvsCaptureVideoFx videoFx;
                if (effect.getIsBasic()) {
                    videoFx = streamingContext.appendBuiltinCaptureVideoFx(effect.getName());
                } else {
                    videoFx = streamingContext.appendPackagedCaptureVideoFx(effect.getUuid());
                }
                return Converters.convertCaptureVideoFx(videoFx);
            default:
                break;
        }
        return null;
    }

    @Override
    public void removeEffectFromCapture(@NonNull MeisheAppliedEffect effect) {
        streamingContext.removeCaptureVideoFx(effect.getIndex().intValue());
    }


    @Override
    public void removeAllEffectOfTypeFromCapture(@NonNull MeisheEffectType type) {
        switch (type) {
            case VIDEO:
                streamingContext.removeAllCaptureVideoFx();
                break;
            case AUDIO:
                streamingContext.removeAllCaptureAudioFx();
                break;
            case STICKER:
                streamingContext.removeAllCaptureAnimatedSticker();
            default:
                break;
        }
    }

    @NonNull
    @Override
    public Double getCaptureEffectFloatParamValue(@NonNull MeisheAppliedEffect effect, @NonNull MeisheEffectParam param) {
        switch (effect.getEffectType()) {
            case VIDEO:
                NvsCaptureVideoFx videoFx = streamingContext.getCaptureVideoFxByIndex(effect.getIndex().intValue());
                return videoFx.getFloatVal(param.getName());
            case AUDIO:
                NvsCaptureAudioFx audioFx = streamingContext.getCaptureAudioFxByIndex(effect.getIndex().intValue());
                return audioFx.getFloatVal(param.getName());
            case STICKER:
                NvsCaptureAnimatedSticker stickerFx = streamingContext.getCaptureAnimatedStickerByIndex(effect.getIndex().intValue());
                return stickerFx.getFloatVal(param.getName());
            default:
                break;
        }
        return 0.0;
    }

    @Override
    public void setCaptureEffectFloatParamValue(@NonNull MeisheAppliedEffect effect, @NonNull MeisheEffectParam param, @NonNull Double value) {
        NvsFx fx;
        switch (effect.getEffectType()) {
            case VIDEO:
                fx = streamingContext.getCaptureVideoFxByIndex(effect.getIndex().intValue());
                break;
            case AUDIO:
                fx = streamingContext.getCaptureAudioFxByIndex(effect.getIndex().intValue());
                break;
            case STICKER:
                fx = streamingContext.getCaptureAnimatedStickerByIndex(effect.getIndex().intValue());
                break;
            default:
                fx = null;
                break;
        }

        fx.setFloatVal(param.getName(), value);
    }

    @NonNull
    @Override
    public MeisheTimelineAppliedEffect addEffectToTimeline(@NonNull MeisheTrackType trackType, @NonNull MeisheEffect effect, @NonNull Long inPoint, @NonNull Long duration) {
        switch (trackType) {
            case VIDEO:
                switch (effect.getEffectType()) {
                    case VIDEO:
                        NvsTimelineVideoFx videoFx;
                        if (effect.getIsBasic()) {
                            videoFx = timeline.addBuiltinTimelineVideoFx(inPoint, duration, effect.getName());
                        } else {
                            videoFx = timeline.addPackagedTimelineVideoFx(inPoint, duration, effect.getUuid());
                        }
                        return new MeisheTimelineAppliedEffect.Builder()
                                .setName(videoFx.getDescription().getName())
                                .setUuid(videoFx.getTimelineVideoFxPackageId())
                                .setIsBasic(videoFx.getTimelineVideoFxPackageId().isEmpty())
                                .setInPoint(videoFx.getInPoint())
                                .setDuration(videoFx.getOutPoint())
                                .setEffectType(effect.getEffectType())
                                .setParams(Utils.getParameters(videoFx.getDescription()))
                                .setParamsValue(Utils.getParametersValues(videoFx))
                                .build();
                    case AUDIO:
                        break;
                    case STICKER:
                        NvsTimelineAnimatedSticker stickerFx;
                        stickerFx = timeline.addAnimatedSticker(inPoint, duration, effect.getUuid());
                        return new MeisheTimelineAppliedEffect.Builder()
                                .setName(stickerFx.getDescription().getName())
                                .setUuid(stickerFx.getAnimatedStickerInAnimationPackageId())
                                .setIsBasic(false)
                                .setInPoint(stickerFx.getInPoint())
                                .setDuration(stickerFx.getOutPoint())
                                .setEffectType(effect.getEffectType())
                                .setParams(Utils.getParameters(stickerFx.getDescription()))
                                .setParamsValue(Utils.getParametersValues(stickerFx))
                                .build();
                    default:
                        break;
                }
                break;
            case AUDIO:
                break;
        }
        return null;
    }

    @Override
    public void removeEffectFromTimeline(@NonNull MeisheTrackType trackType, @NonNull MeisheTimelineAppliedEffect effect) {
        switch (trackType) {
            case VIDEO:
                switch (effect.getEffectType()) {
                    case VIDEO:
                        List<NvsTimelineVideoFx> videoFxs = timeline.getTimelineVideoFxByTimelinePosition(effect.getInPoint() + 1);
                        for (NvsTimelineVideoFx fx : videoFxs) {
                            timeline.removeTimelineVideoFx(fx);
                        }
                        break;
                    case AUDIO:
                        break;
                    case STICKER:
                        List<NvsTimelineAnimatedSticker> stickerFxs = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint() + 1);
                        for (NvsTimelineAnimatedSticker fx : stickerFxs) {
                            timeline.removeAnimatedSticker(fx);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case AUDIO:
                break;
        }
    }

    @Override
    public void resizeTimelineEffect(@NonNull MeisheTimelineAppliedEffect effect, @NonNull Long inPoint, @NonNull Long outPoint) {
        switch (effect.getEffectType()) {
            case VIDEO:
                List<NvsTimelineVideoFx> videoFxs = timeline.getTimelineVideoFxByTimelinePosition(effect.getInPoint() + 1);
                for (NvsTimelineVideoFx fx : videoFxs) {
                    if (effect.getName().equals(fx.getDescription().getName())) {
                        fx.changeInPoint(inPoint);
                        fx.changeOutPoint(outPoint);
                        return;
                    }
                }
                break;
            case AUDIO:
                break;
            case STICKER:
                List<NvsTimelineAnimatedSticker> stickerFxs = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint() + 1);
                for (NvsTimelineAnimatedSticker fx : stickerFxs) {
                    if (effect.getName().equals(fx.getDescription().getName())) {
                        fx.changeInPoint(inPoint);
                        fx.changeOutPoint(outPoint);
                        return;
                    }
                }
                break;
            default:
                break;
        }

    }

    @NonNull
    @Override
    public Double getTimelineEffectFloatParamValue(@NonNull MeisheTrackType trackType, @NonNull MeisheTimelineAppliedEffect effect, @NonNull MeisheEffectParam param) {
        switch (trackType) {
            case VIDEO:
                switch (effect.getEffectType()) {
                    case VIDEO:
                        List<NvsTimelineVideoFx> videoFxs = timeline.getTimelineVideoFxByTimelinePosition(effect.getInPoint() + 1);
                        for (NvsTimelineVideoFx fx : videoFxs) {
                            if (effect.getName().equals(fx.getDescription().getName())) {
                                return fx.getFloatVal(param.getName());
                            }
                        }
                        break;
                    case AUDIO:
                        break;
                    case STICKER:
                        break;
                    default:
                        break;
                }
                break;
            case AUDIO:
                break;
        }
        return 0.0;
    }

    @Override
    public void setTimelineEffectFloatParamValue(@NonNull MeisheTrackType trackType, @NonNull MeisheTimelineAppliedEffect effect, @NonNull MeisheEffectParam param, @NonNull Double value) {
        switch (trackType) {
            case VIDEO:
                switch (effect.getEffectType()) {
                    case VIDEO:
                        List<NvsTimelineVideoFx> videoFxs = timeline.getTimelineVideoFxByTimelinePosition(effect.getInPoint() + 1);
                        for (NvsTimelineVideoFx fx : videoFxs) {
                            if (effect.getName().equals(fx.getDescription().getName())) {
                                fx.setFloatVal(param.getName(), value);
                                return;
                            }
                        }
                        break;
                    case AUDIO:
                        break;
                    case STICKER:
                        List<NvsTimelineAnimatedSticker> stickerFxs = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint() + 1);
                        for (NvsTimelineAnimatedSticker fx : stickerFxs) {
                            if (effect.getName().equals(fx.getDescription().getName())) {
                                fx.setFloatVal(param.getName(), value);
                                return;
                            }
                        }
                        break;
                    default:
                        break;
                }
                break;
            case AUDIO:
                break;
        }
    }

    @NonNull
    @Override
    public MeisheRect getStickerRectFromCapture(@NonNull MeisheAppliedEffect effect) {
        NvsCaptureAnimatedSticker sticker = streamingContext.getCaptureAnimatedStickerByIndex(effect.getIndex().intValue());
        if (sticker == null) {
            return new MeisheRect.Builder()
                    .setLeft(0d)
                    .setRight(0d)
                    .setTop(0d)
                    .setBottom(0d)
                    .build();
        } else {
            android.graphics.RectF r = sticker.getOriginalBoundingRect();

            return new MeisheRect.Builder()
                    .setLeft((double) r.left)
                    .setRight((double) r.right)
                    .setTop((double) r.top)
                    .setBottom((double) r.bottom)
                    .build();
        }
    }

    @NonNull
    @Override
    public MeisheVector2 getStickerPositionFromCapture(@NonNull MeisheAppliedEffect effect) {
        NvsCaptureAnimatedSticker sticker = streamingContext.getCaptureAnimatedStickerByIndex(effect.getIndex().intValue());

        if (sticker == null) {
            return new MeisheVector2.Builder()
                    .setX(0l)
                    .setY(0l)
                    .build();
        } else {
            android.graphics.PointF t = sticker.getTranslation();

            return new MeisheVector2.Builder()
                    .setX((long) t.x)
                    .setY((long) t.y)
                    .build();
        }
    }

    @Override
    public void updateCaptureStickerPosition(@NonNull MeisheAppliedEffect effect, @NonNull MeisheVector2 position) {
        NvsCaptureAnimatedSticker sticker = streamingContext.getCaptureAnimatedStickerByIndex(effect.getIndex().intValue());
        if (sticker == null) return;

        sticker.setTranslation(new PointF(position.getX(), position.getY()));
    }

    @NonNull
    @Override
    public MeisheRect getStickerRectFromTimeline(@NonNull MeisheTimelineAppliedEffect effect) {
        List<NvsTimelineAnimatedSticker> fx = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint());
        if (fx.isEmpty()) {
            return new MeisheRect.Builder()
                    .setLeft(0d)
                    .setRight(0d)
                    .setTop(0d)
                    .setBottom(0d)
                    .build();
        }

        NvsTimelineAnimatedSticker sticker = fx.get(0);

        android.graphics.RectF r = sticker.getOriginalBoundingRect();

        return new MeisheRect.Builder()
                .setLeft((double) r.left)
                .setRight((double) r.right)
                .setTop((double) r.top)
                .setBottom((double) r.bottom)
                .build();
    }

    @NonNull
    @Override
    public MeisheVector2 getStickerPositionFromTimeline(@NonNull MeisheTimelineAppliedEffect effect) {
        List<NvsTimelineAnimatedSticker> fx = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint());
        if (fx.isEmpty()) {
            return new MeisheVector2.Builder()
                    .setX(0l)
                    .setY(0l)
                    .build();
        }

        NvsTimelineAnimatedSticker sticker = fx.get(0);

        android.graphics.PointF t = sticker.getTranslation();

        return new MeisheVector2.Builder()
                .setX((long) t.x)
                .setY((long) t.y)
                .build();
    }

    @Override
    public void updateTimelineStickerPosition(@NonNull MeisheTimelineAppliedEffect effect, @NonNull MeisheVector2 position) {
        List<NvsTimelineAnimatedSticker> fx = timeline.getAnimatedStickersByTimelinePosition(effect.getInPoint());
        if (fx.isEmpty()) return;

        NvsTimelineAnimatedSticker sticker = fx.get(0);
        sticker.setTranslation(new PointF(position.getX(), position.getY()));
    }



/*
    @NonNull
    @Override
    public NvsEffect addEffectOfTypeToTarget(@NonNull NvsEffectType type, @NonNull NvsTarget target, String name, String uuid) {
        switch (type) {
            case VIDEO:
                if (target == NvsTarget.CAPTURE) {
                    if (name != null) {
                        com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.appendBuiltinCaptureVideoFx(name);
                        return new NvsEffect.Builder()
                                .setName(name)
                                .setUuid("")
                                .setIsBasic(true)
                                .setParams(Utils.getParameters(fx.getDescription()))
                                .build();
                    } else {
                        streamingContext.appendPackagedCaptureVideoFx(uuid);
                    }
                } else {
                    if (name != null) {
                        timeline.addBuiltinTimelineVideoFx(0, 3 * 1000000, name);
                    } else {
                        timeline.addPackagedTimelineVideoFx(0, 3 * 1000000, uuid);
                    }
                }
                break;
            case STICKER:
                if (target == NvsTarget.CAPTURE) {
                    if (name != null) {
                        streamingContext.appendCaptureAnimatedSticker(0, 100 * 1000000, name);
                    } else {
                        streamingContext.appendCaptureAnimatedSticker(0, 100 * 1000000, uuid);
                    }
                } else {
                    if (name != null) {
                        timeline.addAnimatedSticker(0, 3 * 1000000, name);
                    } else {
                        timeline.addAnimatedSticker(0, 3 * 1000000, uuid);
                    }
                }
            default:
                break;
        }
        return null;
    }

    @Override
    public void removeEffectOfTypeFromTarget(@NonNull NvsEffectType type, @NonNull NvsTarget target, @NonNull Long index) {
        switch (type) {
            case VIDEO:
                if (target == NvsTarget.CAPTURE) {
                    streamingContext.removeCaptureVideoFx(index.intValue());
                } else {
                }
                break;
            case AUDIO:
                if (target == NvsTarget.CAPTURE) {
                    streamingContext.removeCaptureAudioFx(index.intValue());
                } else {
                }
                break;
            case STICKER:
                if (target == NvsTarget.CAPTURE) {
                    streamingContext.removeCaptureAnimatedSticker(index.intValue());
                } else {
                }
            default:

                break;
        }
    }

    @NonNull
    @Override
    public List<NvsEffect> getAppliedEffectsOfTypeFromTarget(@NonNull NvsEffectType type, @NonNull NvsTarget target) {
        List<NvsEffect> effects = new ArrayList<>();
        switch (type) {
            case VIDEO:
                if (target == NvsTarget.CAPTURE) {
                    for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
                        com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);
                        NvsEffect.Builder builder = new NvsEffect.Builder();

                        effects.add(builder.setName(fx.getDescription().getName())
                                .setUuid(fx.getCaptureVideoFxPackageId())
                                .setIsBasic(false)
                                .setParams(Utils.getParameters(fx.getDescription()))
                                .build()
                        );
                    }
                } else {
                }
                break;
            default:
                break;
        }
        return effects;
    }
*/

    /*
    @NonNull
    @Override
    public Double getFloatParamValue(@NonNull String effectName, @NonNull String param) {
        for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
            com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);

            if (fx.getDescription().getName().equals(effectName)) {
                return fx.getFloatVal(param);
            }
        }

        return (double) -1;
    }

    @NonNull
    @Override
    public Long getIntParamValue(@NonNull String effectName, @NonNull String param) {
        for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
            com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);

            if (fx.getDescription().getName().equals(effectName)) {
                return (long) fx.getIntVal(param);
            }
        }

        return (long) -1;
    }

    @Override
    public void setFloatParamValue(@NonNull String effectName, @NonNull String param, @NonNull Double value) {
        for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
            com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);

            if (fx.getDescription().getName().equals(effectName)) {
                fx.setFloatVal(param, value);
                break;
            }
        }
    }

    @Override
    public void setIntParamValue(@NonNull String effectName, @NonNull String param, @NonNull Long value) {
        for (int i = 0; i < streamingContext.getCaptureVideoFxCount(); i++) {
            com.meicam.sdk.NvsCaptureVideoFx fx = streamingContext.getCaptureVideoFxByIndex(i);

            if (fx.getDescription().getName().equals(effectName)) {
                fx.setIntVal(param, value.intValue());
                break;
            }
        }
    }

    @NonNull
    @Override
    public NvsRect getStickerRect(@NonNull String effectName) {
        for (int i = 0; i < streamingContext.getCaptureAnimatedStickerCount(); i++) {
            com.meicam.sdk.NvsCaptureAnimatedSticker fx = streamingContext.getCaptureAnimatedStickerByIndex(i);
            if (fx.getDescription().getName().equals(effectName)) {
                android.graphics.RectF r = fx.getOriginalBoundingRect();

                return new NvsRect.Builder()
                        .setLeft((double) r.left)
                        .setRight((double) r.right)
                        .setTop((double) r.top)
                        .setBottom((double) r.bottom)
                        .build();

            }
        }

        return new NvsRect.Builder()
                .setLeft(0d)
                .setRight(0d)
                .setTop(0d)
                .setBottom(0d)
                .build();
    }

    @NonNull
    @Override
    public NvsVector2 getStickerPosition(@NonNull String effectName) {
        for (int i = 0; i < streamingContext.getCaptureAnimatedStickerCount(); i++) {
            com.meicam.sdk.NvsCaptureAnimatedSticker fx = streamingContext.getCaptureAnimatedStickerByIndex(i);

            if (fx.getDescription().getName().equals(effectName)) {
                PointF t = fx.getTranslation();
                return new NvsVector2.Builder()
                        .setX((long) t.x)
                        .setY((long) t.y)
                        .build();
            }
        }

        return new NvsVector2.Builder()
                .setX(0l)
                .setY(0l)
                .build();
    }

    @Override
    public void updateStickerPosition(@NonNull String effectName, @NonNull NvsVector2 position) {
        for (int i = 0; i < streamingContext.getCaptureAnimatedStickerCount(); i++) {
            com.meicam.sdk.NvsCaptureAnimatedSticker fx = streamingContext.getCaptureAnimatedStickerByIndex(i);
            if (fx.getDescription().getName().equals(effectName)) {
                fx.setTranslation(new PointF(position.getX(), position.getY()));
                break;
            }
        }
    }
*/


    private byte[] getByteArrayFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }

    @Override
    public void getThumbnail(@NonNull String filePath, @NonNull Long timestamp) {
        NvsIconGenerator _iconGenerator = new NvsIconGenerator();

        Bitmap bmp = _iconGenerator.getIconFromCache(filePath, timestamp, 0);
        if (bmp != null) {
            byte[] b = getByteArrayFromBitmap(bmp);
            nvsCallback.thumbnailGenerated(filePath, b, reply -> {
            });
            return;
        }

        Long id = _iconGenerator.getIcon(filePath, timestamp, 0);
        _iconGenerator.setIconCallback((bitmap, l, l1) -> {
            byte[] b = getByteArrayFromBitmap(bitmap);
            nvsCallback.thumbnailGenerated(filePath, b, reply -> {
            });
        });
    }

    @Override
    public void onStreamingEngineStateChanged(int i) {
        nvsCallback.engineStateChanged((long) i, reply -> {
        });
    }

    @Override
    public void onFirstVideoFramePresented(NvsTimeline nvsTimeline) {
        Log.i("Meishe Plugin", "onFirstVideoFramePresented");
    }

    @Override
    public void onCapturedPictureArrived(ByteBuffer byteBuffer, NvsVideoFrameInfo nvsVideoFrameInfo) {
        final File outputDir = activity.getApplicationContext().getCacheDir();
        try {
            File captureFile = File.createTempFile("CAP_", ".jpg", outputDir);
            Bitmap pictureBitmap = Bitmap.createBitmap(nvsVideoFrameInfo.frameWidth, nvsVideoFrameInfo.frameHeight, Bitmap.Config.ARGB_8888);
            pictureBitmap.copyPixelsFromBuffer(byteBuffer);

            FileOutputStream fOut = new FileOutputStream(captureFile);

            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.close();

            takePictureFuture.complete(captureFile.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onCaptureDeviceCapsReady(int i) {

    }

    @Override
    public void onCaptureDevicePreviewResolutionReady(int i) {

    }

    @Override
    public void onCaptureDevicePreviewStarted(int i) {

    }

    @Override
    public void onCaptureDeviceError(int i, int i1) {

    }

    @Override
    public void onCaptureDeviceStopped(int i) {

    }

    @Override
    public void onCaptureDeviceAutoFocusComplete(int i, boolean b) {

    }

    @Override
    public void onCaptureRecordingFinished(int i) {

    }

    @Override
    public void onCaptureRecordingError(int i) {

    }

    @Override
    public void onCaptureRecordingStarted(int i) {

    }

    @Override
    public void onCaptureRecordingDuration(int i, long l) {
        nvsCallback.videoRecordingDuration((long) i, l, reply -> {
        });
    }

    @Override
    public void onCompileProgress(NvsTimeline nvsTimeline, int i) {
        nvsCallback.compileTimelineProgress((double) i, reply -> {
        });
    }

    @Override
    public void onCompileFinished(NvsTimeline nvsTimeline) {
        nvsCallback.compileComplete(resultFilePath, reply -> {
        });
        resultFilePath = "";
    }

    @Override
    public void onCompileFailed(NvsTimeline nvsTimeline) {

    }

    @Override
    public void onCompileCompleted(NvsTimeline nvsTimeline, boolean b) {

    }

    @Override
    public void onCompileCompleted(NvsTimeline nvsTimeline, boolean b, int i, String s, int i1) {

    }

    @Override
    public void onCompileFloatProgress(NvsTimeline nvsTimeline, float v) {

    }

    @Override
    public void onHardwareError(int i, String s) {

    }

    @Override
    public void onPlaybackPreloadingCompletion(NvsTimeline nvsTimeline) {
        Log.i("Meishe Plugin", "onPlaybackPreloadingCompletion");
    }

    @Override
    public void onPlaybackStopped(NvsTimeline nvsTimeline) {
        Log.i("Meishe Plugin", "onPlaybackStopped");
    }

    @Override
    public void onPlaybackEOF(NvsTimeline nvsTimeline) {
        Log.i("Meishe Plugin", "onPlaybackEOF");
    }

    @Override
    public void onPlaybackTimelinePosition(NvsTimeline nvsTimeline, long l) {
        nvsCallback.playbackTimelinePosition(l, reply -> {
        });
    }

    @Override
    public void onSeekingTimelinePosition(NvsTimeline nvsTimeline, long l) {
        nvsCallback.seekingTimelinePosition(l, reply -> {
        });
    }
}
