package com.example.meishe_plugin;
import com.meicam.sdk.NvsCaptureVideoFx;
import com.example.meishe_plugin.MeishePluginApi.*;
import com.meicam.sdk.NvsFx;

public class Converters {
    static MeisheClip convertClip(com.meicam.sdk.NvsClip clip) {
        return new MeisheClip.Builder()
                .setIndex((long) clip.getIndex())
                .setFilePath(clip.getFilePath())
                .setDuration(clip.getOutPoint() - clip.getInPoint())
                .setClipType(MeisheClipType.values()[clip.getType()])
                .setInPoint(clip.getInPoint())
                .setOutPoint(clip.getOutPoint())
                .setTrimInPoint(clip.getTrimIn())
                .setTrimOutPoint(clip.getTrimOut())
                .setTrackType(MeisheTrackType.values()[clip.getType()])
                .build();
    }

    static MeisheAppliedEffect convertCaptureVideoFx(NvsCaptureVideoFx fx){
        return new MeisheAppliedEffect.Builder()
                .setIndex((long) fx.getIndex())
                .setName(fx.getDescription().getName())
                .setParams(Utils.getParameters(fx.getDescription()))
                .setEffectType(MeisheEffectType.VIDEO)
                .setIsBasic(fx.getCaptureVideoFxPackageId().isEmpty())
                .setParamsValue(Utils.getParametersValues(fx))
                .setUuid(fx.getCaptureVideoFxPackageId())
                .build();
    }
}
