import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

class MeisheClipThumbnailView extends StatelessWidget {
  const MeisheClipThumbnailView({
    super.key,
    required this.clip,
    this.ppm = 48 / 1000000,
  });

  final MeisheClip clip;
  final double ppm;

  @override
  Widget build(BuildContext context) {
    const String viewType = '<MultiThumbnailView>';

    final Map<String, dynamic> creationParams = <String, dynamic>{};
    creationParams["mediaFilePath"] = clip.filePath;
    creationParams["trimIn"] = clip.trimInPoint.toString();
    creationParams["trimOut"] = clip.trimOutPoint.toString();
    creationParams["inPoint"] = "0";
    creationParams["outPoint"] = clip.trimOutPoint.toString();
    // creationParams["inPoint"] = clip.inPoint.toString();
    // creationParams["outPoint"] = clip.outPoint.toString();
    creationParams["ppm"] = ppm.toString();

    return AndroidView(
      viewType: viewType,
      layoutDirection: TextDirection.ltr,
      creationParams: creationParams,
      creationParamsCodec: const StandardMessageCodec(),
    );
  }
}
