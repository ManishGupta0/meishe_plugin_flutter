import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

class MeisheCapturePreview extends StatelessWidget {
  const MeisheCapturePreview({super.key});

  @override
  Widget build(BuildContext context) {
    return Texture(textureId: MeisheEngine.capturePreviewTextureId);
  }
}
