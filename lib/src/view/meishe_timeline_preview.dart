import 'package:flutter/material.dart';
import 'package:meishe_plugin/meishe_plugin.dart';

class MeisheTimelinePreview extends StatelessWidget {
  const MeisheTimelinePreview({super.key});

  @override
  Widget build(BuildContext context) {
    return Texture(textureId: MeisheEngine.editorPreviewTextureId);
  }
}
