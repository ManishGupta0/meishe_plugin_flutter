import 'dart:typed_data';

import 'package:meishe_plugin/src/logic/meishe_engine.dart';

class MeisheUtils {
  static Future<Uint8List?> getThumbnail(String filePath) async {
    final f = MeisheEngine.thumbnailGeneratedStream.firstWhere(
      (element) => element.$1 == filePath,
    );

    await MeisheEngine.api.getThumbnail(filePath, 0);
    final data = await f;

    return data.$2;
  }
}
