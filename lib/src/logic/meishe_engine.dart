import 'dart:async';
import 'dart:developer';
import 'dart:typed_data';

import 'package:meishe_plugin/src/logic/meishe_api.g.dart';
import 'package:meishe_plugin/src/logic/meishe_callbacks.dart';

class MeisheEngine {
  static final MeisheApi api = MeisheApi();
  static bool _init = false;
  static int capturePreviewTextureId = -1;
  static int editorPreviewTextureId = -1;
  static MeisheEngineState _engineState = MeisheEngineState.stopped;

  static final MeisheApiCallback callback = MeisheCallback(
    onEngineStateChanged: _engineStateChanged,
    onVideoRecordingDuration: _videoRecordingDuration,
    onCompileTimelineProgress: _compileTimelineProgress,
    onCompileComplete: _compileComplete,
    onPlaybackTimelinePosition: _playbackTimelinePosition,
    onSeekingTimelinePosition: _seekingTimelinePosition,
    onThumbnailGenerated: _thumbnailGenerated,
  );

  static final _engineStateSC = StreamController<MeisheEngineState>.broadcast();
  static final _videoRecordingDurationSC =
      StreamController<(int, int)>.broadcast();
  static final _compileTimelineProgressSC =
      StreamController<double>.broadcast();
  static final _compileCompleteSC = StreamController<String>.broadcast();
  static final _playbackTimelinePositionSC = StreamController<int>.broadcast();
  static final _seekingTimelinePositionSC = StreamController<int>.broadcast();
  static final _thumbnailGeneratedSC =
      StreamController<(String, Uint8List)>.broadcast();

  /////////////////
  static bool get initialized => _init;
  static MeisheEngineState get engineState => _engineState;
  static int get getCapturePreviewTextureId => capturePreviewTextureId;
  static int get getEditorPreviewTextureId => editorPreviewTextureId;

  static Stream<MeisheEngineState> get engineStateStream =>
      _engineStateSC.stream;
  static Stream<(int, int)> get videoRecordingDurationStream =>
      _videoRecordingDurationSC.stream;
  static Stream<double> get compileTimelineProgressStream =>
      _compileTimelineProgressSC.stream;
  static Stream<String> get compileCompleteStream => _compileCompleteSC.stream;
  static Stream<int> get playbackTimelinePositionStream =>
      _playbackTimelinePositionSC.stream;
  static Stream<int> get seekingTimelinePositionStream =>
      _seekingTimelinePositionSC.stream;
  static Stream<(String, Uint8List)> get thumbnailGeneratedStream =>
      _thumbnailGeneratedSC.stream;

  static Future<bool> init() async {
    _init = await api.init();

    if (_init) {
      capturePreviewTextureId = await api.getCapturePreviewTextureId();
      editorPreviewTextureId = await api.getEditorPreviewTextureId();
      MeisheApiCallback.setup(callback);
    }

    log("Init status: $_init");

    return _init;
  }

  static Future<void> dispose() async {
    await api.dispose();
  }

  // callbacks
  static void _engineStateChanged(MeisheEngineState state) {
    log('Engine State Changed: $state');
    _engineState = state;
    _engineStateSC.add(state);
  }

  static void _videoRecordingDuration(int cameraIndex, int duration) {
    _videoRecordingDurationSC.add((cameraIndex, duration));
  }

  static void _compileTimelineProgress(double progress) {
    _compileTimelineProgressSC.add(progress);
  }

  static void _compileComplete(String filePath) {
    _compileCompleteSC.add(filePath);
  }

  static void _playbackTimelinePosition(int timestamp) {
    _playbackTimelinePositionSC.add(timestamp);
  }

  static void _seekingTimelinePosition(int timestamp) {
    _seekingTimelinePositionSC.add(timestamp);
  }

  static void _thumbnailGenerated(String filePath, Uint8List data) {
    _thumbnailGeneratedSC.add((filePath, data));
  }
}
