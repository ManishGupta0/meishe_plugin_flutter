import 'dart:typed_data';

import 'package:meishe_plugin/src/logic/meishe_api.g.dart';

class MeisheCallback extends MeisheApiCallback {
  MeisheCallback({
    this.onEngineStateChanged,
    this.onVideoRecordingDuration,
    this.onCompileTimelineProgress,
    this.onCompileComplete,
    this.onPlaybackTimelinePosition,
    this.onSeekingTimelinePosition,
    this.onThumbnailGenerated,
  });

  final void Function(MeisheEngineState state)? onEngineStateChanged;
  final void Function(int cameraIndex, int duration)? onVideoRecordingDuration;
  final void Function(double progress)? onCompileTimelineProgress;
  final void Function(String filePath)? onCompileComplete;
  final void Function(int position)? onPlaybackTimelinePosition;
  final void Function(int position)? onSeekingTimelinePosition;
  final void Function(String filePath, Uint8List data)? onThumbnailGenerated;

  @override
  void engineStateChanged(int state) {
    onEngineStateChanged?.call(MeisheEngineState.values[state]);
  }

  @override
  void videoRecordingDuration(int cameraIndex, int duration) {
    onVideoRecordingDuration?.call(cameraIndex, duration);
  }

  @override
  void compileTimelineProgress(double progress) {
    onCompileTimelineProgress?.call(progress);
  }

  @override
  void compileComplete(String filePath) {
    onCompileComplete?.call(filePath);
  }

  @override
  void playbackTimelinePosition(int timestamp) {
    onPlaybackTimelinePosition?.call(timestamp);
  }

  @override
  void seekingTimelinePosition(int timestamp) {
    onSeekingTimelinePosition?.call(timestamp);
  }

  @override
  void thumbnailGenerated(String filePath, Uint8List data) {
    onThumbnailGenerated?.call(filePath, data);
  }
}
