import 'package:meishe_plugin/src/logic/meishe_api.g.dart';
import 'package:meishe_plugin/src/logic/meishe_engine.dart';

class MeisheEditor {
  int duration = 0;

  static MeisheApi get api => MeisheEngine.api;

  static Future<void> _createTimeline({
    required int width,
    required int height,
    required MeisheVector2 aspectRatio,
  }) async {
    await api.createTimeline(width, height, aspectRatio);
  }

  static Future<void> _deleteTimeline() async {
    await api.deleteTimeline();
  }

  bool _playbackStarted = false;
  bool _playbackPaused = false;

  bool get playbackStarted => _playbackStarted;
  bool get playbackPaused => _playbackPaused;

  Future<bool> init({
    required int width,
    required int height,
    required MeisheVector2 aspectRatio,
  }) async {
    if (!MeisheEngine.initialized) {
      await MeisheEngine.init();
    }

    if (!MeisheEngine.initialized) return false;

    await _createTimeline(
      width: width,
      height: height,
      aspectRatio: aspectRatio,
    );

    return true;
  }

  Future<void> dispose() async {
    await _deleteTimeline();
    // MeisheEngine.dispose();
  }

  Future<int> getDuration() async {
    return api.getTimelineDuration();
  }

  Future<void> playTimeline([int startAt = 0]) async {
    await api.playTimeline(startAt);
    _playbackStarted = true;
  }

  Future<void> pauseTimeline() async {
    _playbackPaused = true;
    await api.pauseTimeline();
  }

  Future<void> resumeTimeline() async {
    _playbackPaused = false;
    await api.resumeTimeline();
  }

  Future<bool> isTimelinePaused() async {
    _playbackPaused = await api.isTimelinePaused();
    return _playbackPaused;
  }

  Future<bool> isTimelinePlaying() async {
    return api.isTimelinePlaying();
  }

  Future<void> seekTimeline(int timestamp) async {
    await api.seekTimeline(timestamp);
  }

  updateTimeline() async {}

  Future<void> startTimelineCompiling() async {
    await api.startTimelineCompiling();
  }

  Future<void> cancelTimelineCompiling() async {
    await api.cancelTimelineCompiling();
  }

  Future<void> pauseTimelineCompiling() async {
    await api.pauseTimelineCompiling();
  }

  Future<void> resumeTimelineCompiling() async {
    await api.resumeTimelineCompiling();
  }

  Future<bool> isTimelineCompilingPaused() async {
    return api.isTimelineCompilingPaused();
  }

  Future<MeisheClip> addVideoTrackClip(String filepath) async {
    return api.addClip(MeisheTrackType.video, filepath);
  }

  Future<void> removeFromVideoTrackClip(int clipIndex) async {
    await api.removeClip(MeisheTrackType.video, clipIndex);
  }

  Future<void> moveVideoTrackClip(int clipIndex, int timestamp) async {
    await api.moveClip(MeisheTrackType.video, clipIndex, timestamp);
  }

  Future<void> splitVideoTrackClip(int clipIndex, int timestamp) async {
    await api.splitClip(MeisheTrackType.video, clipIndex, timestamp);
  }

  Future<void> resizeVideoTrackClip(
      int clipIndex, int trimInPoint, int trimOutPoint) async {
    await api.resizeClip(
      MeisheTrackType.video,
      clipIndex,
      trimInPoint,
      trimOutPoint,
    );
  }

  Future<MeisheClip> getVideoTrackClip(int clipIndex) async {
    return api.getClip(MeisheTrackType.video, clipIndex);
  }

  Future<List<MeisheClip>> getVideoTrackClips() async {
    final result = await api.getClips(MeisheTrackType.video);
    return result.map((e) => e!).toList();
  }

  Future<MeisheClip> addAudioTrackClip(String filepath) async {
    return api.addClip(MeisheTrackType.audio, filepath);
  }

  Future<void> removeFromAudioTrackClip(int clipIndex) async {
    await api.removeClip(MeisheTrackType.audio, clipIndex);
  }

  Future<void> moveAudioTrackClip(int clipIndex, int timestamp) async {
    await api.moveClip(MeisheTrackType.audio, clipIndex, timestamp);
  }

  Future<void> splitAudioTrackClip(int clipIndex, int timestamp) async {
    await api.splitClip(MeisheTrackType.audio, clipIndex, timestamp);
  }

  Future<void> resizeAudioTrackClip(
      int clipIndex, int trimInPoint, int trimOutPoint) async {
    await api.resizeClip(
      MeisheTrackType.audio,
      clipIndex,
      trimInPoint,
      trimOutPoint,
    );
  }

  Future<MeisheClip> getClipFromAudioTrackClip(int clipIndex) async {
    return api.getClip(MeisheTrackType.audio, clipIndex);
  }

  Future<List<MeisheClip>> getAudioTrackClips() async {
    final result = await api.getClips(MeisheTrackType.audio);
    return result.map((e) => e!).toList();
  }

  /// streams
  Stream<int> playbackPositionStream() {
    return MeisheEngine.playbackTimelinePositionStream;
  }

  Stream<double> compilingProgressStream() {
    return MeisheEngine.compileTimelineProgressStream;
  }

  Stream<String> compilingCompleteStream() {
    return MeisheEngine.compileCompleteStream;
  }
}
