import 'package:cross_file/cross_file.dart';
import 'package:meishe_plugin/src/logic/meishe_api.g.dart';
import 'package:meishe_plugin/src/logic/meishe_engine.dart';

class MeisheCameraController {
  static MeisheApi get api => MeisheEngine.api;

  static MeisheCameraInfo? _frontCamera;
  static MeisheCameraInfo? _backCamera;
  static final List<MeisheCameraInfo> _cameras = [];

  late MeisheCameraInfo _activeCamera;
  MeisheVector2 _aspectRatio = MeisheVector2(x: 1, y: 1);

  static Future<List<MeisheCameraInfo>> _loadCameraInfos() async {
    final result = await api.getCameras();

    _cameras.clear();

    _cameras.addAll(
      result.map(
        (camera) {
          var c = camera!;

          if (_frontCamera == null &&
              c.lensDirection == MeisheCameraLensDirection.front) {
            _frontCamera = c;
          }
          if (_backCamera == null &&
              c.lensDirection == MeisheCameraLensDirection.back) {
            _backCamera = c;
          }

          return c;
        },
      ),
    );

    return _cameras;
  }

  List<MeisheCameraInfo> getCameras() => _cameras;
  MeisheVector2 get aspectRatio => _aspectRatio;

  Future<bool> init() async {
    if (!MeisheEngine.initialized) {
      await MeisheEngine.init();
    }

    if (!MeisheEngine.initialized) return false;

    await _loadCameraInfos();

    if (_backCamera != null) {
      _activeCamera = _backCamera!;
    } else if (_frontCamera != null) {
      _activeCamera = _frontCamera!;
    } else {
      throw Exception('No Camera Found');
    }

    return true;
  }

  Future<void> dispose() async {
    await api.stopCamera();
  }

  Future<void> startCamera([MeisheVector2? aspectRatio]) async {
    if (aspectRatio != null) {
      _aspectRatio = aspectRatio;
    }

    await api.startCamera(_activeCamera.index, _aspectRatio);
  }

  Future<void> setAspectRatio(MeisheVector2 aspectRatio) async {
    await startCamera(aspectRatio);
  }

  Future<void> flipCamera() async {
    if (_activeCamera.lensDirection == MeisheCameraLensDirection.back &&
        _frontCamera != null) {
      _activeCamera = _frontCamera!;
      await startCamera();
      return;
    }

    if (_activeCamera.lensDirection == MeisheCameraLensDirection.front &&
        _backCamera != null) {
      _activeCamera = _backCamera!;
      await startCamera();
      return;
    }
  }

  Future<void> setFlashMode(MeisheCameraFlashMode flashModel) async {
    await api.setFlashMode(flashModel);
  }

  Future<XFile> takePicture() async {
    final filePath = await api.takePicture(_activeCamera.index);
    return XFile(filePath, mimeType: 'image/jpeg');
  }

  Future<bool> startVideoRecording() async {
    return api.startVideoRecording(_activeCamera.index, _aspectRatio);
  }

  Future<XFile> stopVideoRecording() async {
    final filePath = await api.stopVideoRecording();
    return XFile(filePath, mimeType: "video/mp4");
  }

  Future<void> pauseVideoRecording() async {
    await api.pauseVideoRecording();
  }

  Future<void> resumeVideoRecording() async {
    await api.resumeVideoRecording();
  }

  Future<bool> isVideoRecoringPaused() async {
    return api.isVideoRecordingPaused();
  }

  Stream<int> videoRecordingDurationStream() {
    return MeisheEngine.videoRecordingDurationStream
        .map<int>((event) => event.$2);
  }
}
