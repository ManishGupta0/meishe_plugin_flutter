enum MeisheEngineState {
  stopped,
  capturePreview,
  captureRecording,
  playback,
  seeking,
  compile,
}

enum MeisheCameraDirection {
  front,
  back,
}

enum MeisheCameraFlashMode {
  off,
  on,
  auto,
  tourch,
}

enum MeisheTrackType {
  audio,
  video,
}

enum MeisheEffectType {
  unknown,
  audio,
  video,
  sticker,
  videoTransition,
}

enum MeisheEffectParamType {
  int,
  float,
  bool,
  string,
  unknowm,
}

class MeisheVector {
  const MeisheVector({
    required this.x,
    required this.y,
  });

  final int x;
  final int y;
}

class MeisheRect {
  MeisheRect({
    required this.left,
    required this.right,
    required this.top,
    required this.bottom,
  });

  final double left;
  final double right;
  final double top;
  final double bottom;
}

enum MeisheAssetTypeype {
  videoFx,
  videoTransition,
  captionStyle,
  animatedSticker,
  theme,
  captureScene,
  arScene,
}

class CameraInfo {}

class MeisheEngine {
  MeisheEngineState state = MeisheEngineState.stopped;
  static init() {}
  static dispose() {}
}

abstract class MeisheCamera {
  static void getCameras() {}
}

class MeisheClipInfo {}

class MeisheFileInfo {
  MeisheFileInfo({
    required this.name,
    required this.duration,
    required this.size,
  });

  final String name;
  final int duration;
  final MeisheVector size;
}

class MeisheTransformEffect {
  MeisheTransformEffect({
    required this.position,
    required this.scale,
    required this.rotation,
  });

  MeisheVector position;
  MeisheVector scale;
  double rotation;
}

class MeisheClip {
  MeisheClip({
    required this.fileInfo,
  });

  MeisheFileInfo fileInfo;

  // final int index;
  // final int inPoint;
  // final int outPoint;
  // final int trimPoint;
  // final int trimOutPoint;
  // MeisheTrackType trackType;
  // bool hasTransition;

  // MeisheTransformEffect transform;

  void resize(int trimIn, int trimOut) {}
}

class MeisheTrack {
  MeisheTrack({
    required this.type,
    required this.clips,
  });

  final MeisheTrackType type;
  final List<MeisheClip> clips;

  void addClip(String filePath, int start, int end) {}
  void getClips() {}
  void deleteClip(MeisheClip clip) {}
}

class MeisheTimeline {
  // aspectRatio
  /// List<MeisheTrack> tracks
  /// MeisheTrack get videoTrack => _videoTrack;
  /// MeisheTrack get audioTrack => _audioTrack;
}

class MeisheEffect {}

class MeisheVideoEffect extends MeisheEffect {}
