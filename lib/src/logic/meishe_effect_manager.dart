import 'package:meishe_plugin/src/logic/meishe_api.g.dart';
import 'package:meishe_plugin/src/logic/meishe_engine.dart';

class MeisheEffectManager {
  static MeisheApi get api => MeisheEngine.api;
  //////////////////////////////////////////////////////////////////////////////
  // asset package manager
  static Future<MeisheEffect> instllAssetOfType(
    MeisheEffectType type,
    String filePath,
  ) async {
    return api.instllAssetOfType(type, filePath);
  }

  static Future<void> uninstallAssetOfType(
    MeisheEffectType type,
    String assetId,
  ) async {
    await api.uninstallAssetOfType(type, assetId);
  }

  static Future<bool> isAssetOfTypeInstalled(
    MeisheEffectType type,
    String assetId,
  ) async {
    return api.isAssetOfTypeInstalled(type, assetId);
  }

  //////////////////////////////////////////////////////////////////////////////
  // get effects
  static Future<List<MeisheEffect>> getBasicEffectsOfType(
      MeisheEffectType type) async {
    final result = await api.getBasicEffectsOfType(type);
    return result.map((e) => e!).toList();
  }

  static Future<List<MeisheEffect>> getInstalledEffectsOfType(
    MeisheEffectType type,
  ) async {
    final result = await api.getInstalledEffectsOfType(type);
    return result.map((e) => e!).toList();
  }

  static Future<List<MeisheAppliedEffect>> getAllAppliedEffectOfTypeFromCapture(
    MeisheEffectType type,
  ) async {
    final result = await api.getAllAppliedEffectOfTypeFromCapture(type);

    return result.map((e) => e!).toList();
  }

  static Future<List<MeisheTimelineAppliedEffect>>
      getAllEffectsOfTypeFromTimeline(
    MeisheEffectType type,
  ) async {
    final result = await api.getAllAppliedEffectOfTypeFromTimeline(
      MeisheTrackType.video,
      type,
    );

    return result.map((e) => e!).toList();
  }

  //////////////////////////////////////////////////////////////////////////////
  /// capture
  static Future<MeisheAppliedEffect> addEffectToCapture(
    MeisheEffect effect,
  ) async {
    return api.addEffectToCapture(effect);
  }

  static Future<void> removeEffectFromCapture(
    MeisheAppliedEffect effect,
  ) async {
    await api.removeEffectFromCapture(effect);
  }

  static Future<void> removeAllEffectFromCapture() async {
    for (var type in MeisheEffectType.values) {
      await removeAllEffectOfTypeFromCapture(type);
    }
  }

  static Future<void> removeAllEffectOfTypeFromCapture(
    MeisheEffectType type,
  ) async {
    await api.removeAllEffectOfTypeFromCapture(type);
  }

  /// timeline
  static Future<MeisheTimelineAppliedEffect> addEffectToTimeline(
    MeisheTrackType trackType,
    MeisheEffect effect,
    int inPoint,
    int duration,
  ) async {
    return api.addEffectToTimeline(trackType, effect, inPoint, duration);
  }

  static Future<void> removeEffectFromTimeline(
    MeisheTrackType trackType,
    MeisheTimelineAppliedEffect effect,
  ) async {
    await api.removeEffectFromTimeline(trackType, effect);
  }

  //////////////////////////////////////////////////////////////////////////////
  // value capture
  static Future<double> getCaptureFloatParamValue({
    required MeisheAppliedEffect effect,
    required MeisheEffectParam param,
  }) async {
    return api.getCaptureEffectFloatParamValue(effect, param);
  }

  static Future<void> setCaptureFloatParamValue({
    required MeisheAppliedEffect effect,
    required MeisheEffectParam param,
    required double value,
  }) async {
    await api.setCaptureEffectFloatParamValue(
      effect,
      param,
      value,
    );
  }

  // value timeline
  static Future<double> getTimelineFloatParamValue({
    required MeisheTrackType trackType,
    required MeisheTimelineAppliedEffect effect,
    required MeisheEffectParam param,
  }) async {
    return api.getTimelineEffectFloatParamValue(trackType, effect, param);
  }

  static Future<void> setTimelineFloatParamValue({
    required MeisheTrackType trackType,
    required MeisheTimelineAppliedEffect effect,
    required MeisheEffectParam param,
    required double value,
  }) async {
    await api.setTimelineEffectFloatParamValue(
      trackType,
      effect,
      param,
      value,
    );
  }
}
